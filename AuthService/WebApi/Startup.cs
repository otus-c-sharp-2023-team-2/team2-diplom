
using Infrastructure.EntityFramework;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Services.Contracts;
using System;
using System.IO;
using System.Reflection;
using System.Security.Cryptography;
using WebApi.Middlewares;

namespace WebApi;

public class Startup
{
    public Startup(IConfiguration configuration)
    {
        Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {
		var jwtSettingsConfiguration = Configuration.GetSection("JwtSettings");
		services.Configure<JWTTokenSettings>(jwtSettingsConfiguration);
		var jwtSettings = jwtSettingsConfiguration.Get<JWTTokenSettings>();

		RSA rsa = RSA.Create();
		rsa.ImportRSAPublicKey(
			source: Convert.FromBase64String(jwtSettings.AccessTokenSettings.PublicKey),
			bytesRead: out int _
		);

		var rsaKey = new RsaSecurityKey(rsa);

		services.AddAuthentication(options =>
		{
			options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
			options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
		})
		.AddJwtBearer(options =>
		{
			options.RequireHttpsMetadata = false;
			options.TokenValidationParameters = new TokenValidationParameters
			{
				ValidateIssuer = true,
				ValidateAudience = true,
				ValidateLifetime = true,
				RequireSignedTokens = true,
				RequireExpirationTime = true,
				ValidateIssuerSigningKey = true,
				ValidIssuer = jwtSettings.AccessTokenSettings.Issuer,
				ValidAudience = jwtSettings.AccessTokenSettings.Audience,
				IssuerSigningKey = rsaKey,
				ClockSkew = TimeSpan.FromMinutes(1)
			};
		});

		services.AddAuthorization();

		services.AddServices(Configuration);
        services.AddControllers();		

		// Register the Swagger generator, defining 1 or more Swagger documents
		services.AddSwaggerGen(options =>
        {
            var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
            options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename));

			var jwtSecurityScheme = new OpenApiSecurityScheme
			{
				BearerFormat = "JWT",
				Name = "JWT Authentication",
				In = ParameterLocation.Header,
				Type = SecuritySchemeType.Http,
				Scheme = JwtBearerDefaults.AuthenticationScheme,
				Description = "Put **_ONLY_** your JWT Bearer token on textbox below!",

				Reference = new OpenApiReference
				{
					Id = JwtBearerDefaults.AuthenticationScheme,
					Type = ReferenceType.SecurityScheme
				}
			};

			options.AddSecurityDefinition(jwtSecurityScheme.Reference.Id, jwtSecurityScheme);

			options.AddSecurityRequirement(new OpenApiSecurityRequirement
	        {
		        { jwtSecurityScheme, Array.Empty<string>() }
	        });
		});

        services.AddCors();
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IWebHostEnvironment env, DatabaseContext context)
    {
        context.Database.EnsureCreated();

        if (env.IsDevelopment())
        {
            app.UseDeveloperExceptionPage();
        }

        app.UseMiddleware<HttpExceptionMiddleware>();

        app.UseCors(builder => builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
        app.UseHealthChecks("/health");

        app.UseRouting();

		app.UseAuthentication();
        app.UseAuthorization();

        if (!env.IsProduction())
        {
            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
                c.RoutePrefix = string.Empty;
            });
        }

        app.UseEndpoints(endpoints =>
        {
            endpoints.MapControllers();
        });
    }

}