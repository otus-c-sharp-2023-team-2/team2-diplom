﻿using Domain.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Net.Http.Headers;
using System;
using System.Net.Mime;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace WebApi.Middlewares;

/// <summary>
/// Middleware для обработки http исключений.
/// </summary>
public class HttpExceptionMiddleware
{
    private readonly RequestDelegate next;
    private readonly ILogger _logger;

    public HttpExceptionMiddleware(RequestDelegate next, ILoggerFactory loggerFactory)
    {
        this.next = next;
        _logger = loggerFactory.CreateLogger<HttpExceptionMiddleware>();
    }

    public async Task Invoke(HttpContext context)
    {
        try
        {
            await this.next.Invoke(context);
        }
        catch (HttpExceptionBase httpException)
        {
            _logger.LogError(httpException, httpException.Message);

            context.Response.Clear();
            context.Response.StatusCode = httpException.StatusCode;

            var mediaType = new MediaTypeHeaderValue("application/json");
            mediaType.Encoding = Encoding.UTF8;
            context.Response.ContentType = mediaType.ToString();

            await context.Response.Body.WriteAsync(
                   JsonSerializer.SerializeToUtf8Bytes(new
                   {
                       ErrorMessage = httpException.Message,
                   }));
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, ex.Message);

            context.Response.Clear();
            context.Response.StatusCode = StatusCodes.Status500InternalServerError;
            context.Response.ContentType = MediaTypeNames.Application.Json;

            await context.Response.Body.WriteAsync(
                    JsonSerializer.SerializeToUtf8Bytes(new
                    {
                        ErrorMessage = ex.Message,
                    }));
        }
    }
}
