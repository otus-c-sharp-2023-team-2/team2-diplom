﻿using Infrastructure.EntityFramework;
using Infrastructure.Repositories.Implementations;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.IdentityModel.Tokens;
using Services.Abstractions;
using Services.Contracts;
using Services.Implementations.Commands;
using Services.Repositories.Abstractions;
using System;
using System.Security.Cryptography;
using WebApi.Settings;

namespace WebApi
{
    /// <summary>
    /// Регистратор сервиса
    /// </summary>
    public static class Registrar
    {
        public static IServiceCollection AddServices(this IServiceCollection services, IConfiguration configuration)
        {
            var applicationSettings = configuration.Get<ApplicationSettings>();

            services.AddSingleton(applicationSettings);
            return services.AddSingleton((IConfigurationRoot)configuration)
                .ConfigureServices((IConfigurationRoot)configuration)
                .ConfigureContext(applicationSettings.ConnectionString);
        }

        private static IServiceCollection ConfigureServices(this IServiceCollection services, IConfiguration configuration)
        {
            var jwtSettingsConfiguration = configuration.GetSection("JwtSettings");
            services.Configure<JWTTokenSettings>(jwtSettingsConfiguration);
            var jwtSettings = jwtSettingsConfiguration.Get<JWTTokenSettings>();

            services.AddSingleton(provider =>
            {
                var rsa = RSA.Create();
                rsa.ImportRSAPrivateKey(source: Convert.FromBase64String(jwtSettings.AccessTokenSettings.PrivateKey), bytesRead: out int _);
                return new RsaSecurityKey(rsa);
            });

			services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

			services.AddSingleton<IAuthTokenService, JWTAuthTokenService>();

            services.AddScoped<IAuthRepository, AuthRepository>();

            // Register use cases
            services.AddScoped<LoginCommand>();
            services.AddScoped<RefreshTokenCommand>();
            services.AddScoped<SignOutCommand>();
            services.AddScoped<CreateUserCommand>();
			services.AddScoped<UpdateUserCommand>();
            services.AddScoped<GetUsersCommand>();

            return services;
        }
    }
}