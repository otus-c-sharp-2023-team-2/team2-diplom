﻿using Domain.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.Contracts.CreateUser;
using Services.Contracts.Login;
using Services.Contracts.RefreshToken;
using Services.Contracts.SignOut;
using Services.Implementations.Commands;
using System.Threading.Tasks;

namespace WebApi.Controllers;

[Route("[controller]")]
[ApiController]
public class AuthController : ControllerBase
{
    private readonly LoginCommand _loginCommand;
    private readonly RefreshTokenCommand _refreshCommand;
    private readonly SignOutCommand _signOutCommand;

    public AuthController(
        LoginCommand loginCommand,
        RefreshTokenCommand refreshTokenCommand,
        SignOutCommand signOutCommand)
    {
        _loginCommand = loginCommand;
        _refreshCommand = refreshTokenCommand;
        _signOutCommand = signOutCommand;
    }

    [AllowAnonymous]
    [HttpPost]
    [Route("Login")]
    public async Task<LoginResponse> Login(LoginRequest request)
    {
        return await _loginCommand.Execute(request);
    }


    [AllowAnonymous]
    [HttpPost]
    [Route("RefreshToken")]
    public async Task<RefreshTokenResponse> RefreshToken(RefreshTokenRequest request)
    {
        return await _refreshCommand.Execute(request);
    }

    [AllowAnonymous]
    [HttpPost]
    [Route("SignOut")]
    public async Task<SignOutResponse> SignOut(SignOutRequest request)
    {
        return await _signOutCommand.Execute(request);
    }
}
