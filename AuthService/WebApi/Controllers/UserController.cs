﻿using Domain.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.Contracts.CreateUser;
using Services.Contracts.Login;
using Services.Contracts.RefreshToken;
using Services.Contracts.SignOut;
using Services.Implementations.Commands;
using System.Threading.Tasks;

namespace WebApi.Controllers;

[Route("[controller]")]
[ApiController]
public class UserController : ControllerBase
{
    private readonly LoginCommand _loginCommand;
    private readonly RefreshTokenCommand _refreshCommand;
    private readonly SignOutCommand _signOutCommand;
    private readonly CreateUserCommand _createUserCommand;
	private readonly UpdateUserCommand _updateUserCommand;
    private readonly GetUsersCommand _getUsersCommand;

    public UserController(
        LoginCommand loginCommand,
        RefreshTokenCommand refreshTokenCommand,
        SignOutCommand signOutCommand,
        CreateUserCommand createUserCommand,
		UpdateUserCommand updateUserCommand,
        GetUsersCommand getUsersCommand)
    {
        _loginCommand = loginCommand;
        _refreshCommand = refreshTokenCommand;
        _signOutCommand = signOutCommand;
        _createUserCommand = createUserCommand;
		_updateUserCommand = updateUserCommand;
        _getUsersCommand = getUsersCommand;
    }  

    [AllowAnonymous]
    [HttpPost]
    public async Task<CreateUserResponse> CreateUser(CreateUserRequest request)
    {
        return await _createUserCommand.Execute(request);
    }

	[Authorize]
	[HttpPut]
	public async Task<UpdateUserResponse> UpdateUser(UpdateUserRequest request)
	{
		return await _updateUserCommand.Execute(request);
	}

    [Authorize(Roles=nameof(Roles.Admin))]
    [HttpGet]
    public async Task<GetUsersRolesListResponse> GetUsersRolesList()
    {
        return await _getUsersCommand.Execute(new GetUsersRolesListRequest());
    }
}
