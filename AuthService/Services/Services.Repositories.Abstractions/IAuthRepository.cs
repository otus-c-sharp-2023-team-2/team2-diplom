﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.Repositories.Abstractions;

public interface IAuthRepository
{
    Task<User> GetUserByEmail(string email);
    Task UpdateUser(User user);
    Task<User> GetUserById(Guid userId);
    Task CreateUser(User user);
    Task<IEnumerable<User>> GetUsersRolesList();
}
