﻿using Domain.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Services.Abstractions.Commands;
using Services.Contracts.CreateUser;
using Services.Implementations.Extensions;
using Services.Implementations.Helpers;
using Services.Repositories.Abstractions;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Services.Implementations.Commands;

public class UpdateUserCommand : IUpdateUserCommand
{
	private readonly ILogger _logger;
	private readonly IAuthRepository _authRepository;
	private readonly IHttpContextAccessor _contextAccessor;

	public UpdateUserCommand(
		ILogger<UpdateUserCommand> logger,
		IAuthRepository authRepository,
		IHttpContextAccessor contextAccessor)
	{
		_logger = logger;
		_authRepository = authRepository;
		_contextAccessor = contextAccessor;
	}

	public async Task<UpdateUserResponse> Execute(UpdateUserRequest request)
	{
		var currentUserId = _contextAccessor?.HttpContext?.User?.Claims?.ToUserId();

		var currentUserRoles = _contextAccessor?.HttpContext?.User?.Claims?.ToRoles();
		var currentUserMaxRole = Roles.None;
		if (currentUserRoles.Any())
			currentUserMaxRole = currentUserRoles.Max();

		if (currentUserId != request.UserId && currentUserMaxRole <= Roles.Visitor)
			throw new AccessViolationException("Недостаточно прав для внемения изменений.");

		var user = await _authRepository.GetUserById(request.UserId);

		if (request.IsActive.HasValue)
			user.Active = request.IsActive.Value;

		if (!string.IsNullOrEmpty(request.Email))
			user.Email = request.Email;

		if (!string.IsNullOrEmpty(request.Name))
			user.Name = request.Name;

		if (!string.IsNullOrEmpty(request.LastName))
			user.LastName = request.LastName;

		if (!string.IsNullOrEmpty(request.Password))
		{
			var salt = CryptographyHelper.GenerateSalt();
			user.Password = CryptographyHelper.HashPassword(request.Password, salt);
			user.Salt = salt;
		}

		if (request.Claims is not null)
		{
			var requestedRoles = request.Claims?.ToRoles();
			var requestedMaxRole = requestedRoles.Any() ? requestedRoles.Max() : Roles.None;
			if (requestedMaxRole > currentUserMaxRole || currentUserMaxRole <= Roles.Visitor)
			{
				throw new AccessViolationException("Запрошенная роль превышает имеющиеся полномочия");
			}
			user.Claims = request.Claims.ToCustomClaims().ToList();
		}

		await _authRepository.UpdateUser(user);

		return new UpdateUserResponse();
	}
}
