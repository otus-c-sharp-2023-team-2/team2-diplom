﻿using Microsoft.Extensions.Logging;
using Services.Abstractions;
using Services.Abstractions.Commands;
using Services.Contracts.RefreshToken;
using Services.Implementations.Exceptions;
using Services.Repositories.Abstractions;
using System;
using System.Threading.Tasks;

namespace Services.Implementations.Commands;

public class RefreshTokenCommand : IRefreshTokenCommand
{
    private readonly ILogger _logger;
    private readonly IAuthTokenService _authTokenService;
    private readonly IAuthRepository _authRepository;

    public RefreshTokenCommand(
        ILogger<RefreshTokenCommand> logger,
        IAuthTokenService authTokenService,
        IAuthRepository authRepository)
    {
        _logger = logger;
        _authTokenService = authTokenService;
        _authRepository = authRepository;
    }

    public async Task<RefreshTokenResponse> Execute(RefreshTokenRequest request)
    {
        if (!_authTokenService.IsTokenValid(request.AccessToken, false))
            throw new InvalidTokenException();


        var userId = _authTokenService.GetUserIdFromToken(request.AccessToken);
        var user = await _authRepository.GetUserById(userId);

        if (!user.RefreshToken?.Active ?? true)
            throw new RefreshTokenIsNotActiveException();

        if (user.RefreshToken.ExpirationDate < DateTime.UtcNow)
            throw new RefreshTokenHasExpiredException(user.RefreshToken.ExpirationDate);

        if (user.RefreshToken.Value != request.RefreshToken)
            throw new InvalidRefreshTokenException();

        var newToken = _authTokenService.GenerateAccessToken(user);

        user.RefreshToken.Value = _authTokenService.GenerateRefreshToken();
        user.RefreshToken.Active = true;
        user.RefreshToken.ExpirationDate = DateTime.UtcNow.AddMinutes(_authTokenService.GetRefreshTokenLifetimeInMinutes());
        await _authRepository.UpdateUser(user);

        var response = new RefreshTokenResponse
        {
            AccessToken = newToken,
            RefreshToken = user.RefreshToken.Value,
            RefreshTokenExpirationDate = user.RefreshToken.ExpirationDate
        };

        return response;
    }
}
