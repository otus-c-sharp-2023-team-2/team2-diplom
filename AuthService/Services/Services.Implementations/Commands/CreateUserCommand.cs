﻿using Domain.Entities;
using Microsoft.Extensions.Logging;
using Services.Abstractions.Commands;
using Services.Contracts.CreateUser;
using Services.Implementations.Extensions;
using Services.Implementations.Helpers;
using Services.Repositories.Abstractions;
using System;
using System.Threading.Tasks;

namespace Services.Implementations.Commands;

public class CreateUserCommand : ICreateUserCommand
{
	private readonly ILogger _logger;
	private readonly IAuthRepository _authRepository;

	public CreateUserCommand(
		ILogger<CreateUserCommand> logger,
		IAuthRepository authRepository)
	{
		_logger = logger;
		_authRepository = authRepository;
	}

	public async Task<CreateUserResponse> Execute(CreateUserRequest request)
	{
		var salt = CryptographyHelper.GenerateSalt();
		var currentDate = DateTime.UtcNow;

		var user = new User
		{
			Id = Guid.NewGuid(),
			Active = true,
			Email = request.Email,
			Password = CryptographyHelper.HashPassword(request.Password, salt),
			Salt = salt,
			Name = request.Name,
			LastName = request.LastName,
			CreationDate = currentDate,
			UpdateDate = currentDate
		};

		await _authRepository.CreateUser(user);

		return new CreateUserResponse
		{
			UserId = user.Id
		};
	}
}
