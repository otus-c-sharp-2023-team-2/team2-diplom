﻿using Domain.Entities;
using Microsoft.Extensions.Logging;
using Services.Abstractions;
using Services.Abstractions.Commands;
using Services.Contracts.Login;
using Services.Implementations.Exceptions;
using Services.Implementations.Helpers;
using Services.Repositories.Abstractions;
using System;
using System.Threading.Tasks;

namespace Services.Implementations.Commands;

public class LoginCommand : ILoginCommand
{
    private readonly ILogger _logger;
    private readonly IAuthTokenService _authTokenService;
    private readonly IAuthRepository _authRepository;

    public LoginCommand(
        ILogger<LoginCommand> logger,
        IAuthTokenService authTokenService,
        IAuthRepository authRepository)
    {
        _logger = logger;
        _authTokenService = authTokenService;
        _authRepository = authRepository;
    }

    public async Task<LoginResponse> Execute(LoginRequest request)
    {
        var user = await _authRepository.GetUserByEmail(request.Email);

        if (!AreCredentialsValid(request.Password, user))
            throw new InvalidCredentialsException();

        var token = _authTokenService.GenerateAccessToken(user);

        user.RefreshToken = new RefreshToken
        {
            Value = _authTokenService.GenerateRefreshToken(),
            Active = true,
            ExpirationDate = DateTime.UtcNow.AddMinutes(_authTokenService.GetRefreshTokenLifetimeInMinutes()),
        };
        await _authRepository.UpdateUser(user);

        var idToken = _authTokenService.GenerateIdToken(user);
        var accessToken = _authTokenService.GenerateAccessToken(user);

        var response = new LoginResponse
        {
            IdToken = idToken,
            AccessToken = accessToken,
            RefreshToken = user.RefreshToken.Value
        };

        return response;
    }

    private bool AreCredentialsValid(string testPassword, User user)
    {
        var hash = CryptographyHelper.HashPassword(testPassword, user.Salt);
        return hash == user.Password;
    }
}
