﻿using Domain.Entities;
using Microsoft.Extensions.Logging;
using Services.Abstractions.Commands;
using Services.Contracts.CreateUser;
using Services.Repositories.Abstractions;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Services.Implementations.Commands;

public class GetUsersCommand : IGetUsersCommand
{
    private readonly ILogger _logger;
    private readonly IAuthRepository _authRepository;

    public GetUsersCommand(
        ILogger<UpdateUserCommand> logger,
        IAuthRepository authRepository)
    {
        _logger = logger;
        _authRepository = authRepository;
    }

    public async Task<GetUsersRolesListResponse> Execute(GetUsersRolesListRequest request)
    {
        var result = await _authRepository.GetUsersRolesList();

        return new GetUsersRolesListResponse()
        {
            Users = result.Select(x => new Domain.Entities.User()
            {
                Id = x.Id,
                Claims = x.Claims
                            .Where(cl => cl.Type == ClaimTypes.Role)
                            .Select(cl => new CustomClaim() { Type = ClaimTypes.Role, Value = cl.Value })
                            .ToList()
            })
        };
    }
}
