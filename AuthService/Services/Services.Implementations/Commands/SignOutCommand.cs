﻿using Microsoft.Extensions.Logging;
using Services.Abstractions.Commands;
using Services.Contracts.SignOut;
using Services.Repositories.Abstractions;
using System;
using System.Threading.Tasks;

namespace Services.Implementations.Commands;

public class SignOutCommand : ISignOutCommand
{
    private readonly ILogger _logger;
    private readonly IAuthRepository _authRepository;

    public SignOutCommand(
        ILogger<SignOutCommand> logger,
        IAuthRepository authRepository)
    {
        _logger = logger;
        _authRepository = authRepository;
    }

    public async Task<SignOutResponse> Execute(SignOutRequest request)
    {
        var user = await _authRepository.GetUserById(request.UserId);

        if (user.RefreshToken is not null)
        {
            user.RefreshToken.Active = false;
            await _authRepository.UpdateUser(user);
        }        

        return new SignOutResponse
        {
            Message = $"User signed out at {DateTime.UtcNow} server time."
        };
    }
}
