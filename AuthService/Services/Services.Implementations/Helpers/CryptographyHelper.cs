﻿using System.Security.Cryptography;
using System.Text;
using System;

namespace Services.Implementations.Helpers;

public static class CryptographyHelper
{
    private const int _hashSize = 64;

    public static string GenerateSalt()
    {
        var buffer = new byte[_hashSize];
        using var rng = new RNGCryptoServiceProvider();
        rng.GetBytes(buffer);
        return Convert.ToBase64String(buffer);
    }

    public static string HashPassword(string password, string salt)
    {
        byte[] data = Encoding.UTF8.GetBytes(string.Concat(password, salt));
        SHA256 cryptoAlg = SHA256.Create();
        var result = cryptoAlg.ComputeHash(data);

        return Convert.ToBase64String(result);
    }
}
