﻿using Domain.Entities;
using Microsoft.IdentityModel.JsonWebTokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace Services.Implementations.Extensions;

public static class ClaimExtensions
{
	public static IEnumerable<CustomClaim> ToCustomClaims(this IEnumerable<Contracts.Claim> requestClaims)
	{
		if (requestClaims == null) 
			return null;

		return requestClaims.Select(r => new CustomClaim { Type = r.Type, Value = r.Value })?.ToList();
	}

	public static Guid ToUserId(this IEnumerable<Claim> claims)
	{
		var idClaim = claims?.FirstOrDefault(x => string.Equals(x.Type, ClaimTypes.NameIdentifier, StringComparison.OrdinalIgnoreCase)
			|| string.Equals(x.Type, JwtRegisteredClaimNames.NameId, StringComparison.OrdinalIgnoreCase))?.Value;
		return !string.IsNullOrEmpty(idClaim) && Guid.TryParse(idClaim, out Guid result) ? result : Guid.Empty;
	}

	public static IEnumerable<Roles> ToRoles(this IEnumerable<Claim> claims)
	{
		if(claims == null) 
			return null;

		var result = new List<Roles>();
		var roles = claims.Where(c => c.Type == ClaimTypes.Role)
			.Select(c => c.Value);

		foreach (var role in roles)
		{
			if(Enum.TryParse(role, out Roles parsedRole))
				result.Add(parsedRole);
		}
		return result;
	}

	public static IEnumerable<Roles> ToRoles(this IEnumerable<Contracts.Claim> claims)
	{
		if (claims == null)
			return null;

		var result = new List<Roles>();
		var roles = claims.Where(c => c.Type == ClaimTypes.Role)
			.Select(c => c.Value);

		foreach (var role in roles)
		{
			if (Enum.TryParse(role, out Roles parsedRole))
				result.Add(parsedRole);
		}
		return result;
	}
}
