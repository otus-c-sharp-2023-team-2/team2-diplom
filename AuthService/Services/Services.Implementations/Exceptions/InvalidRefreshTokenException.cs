﻿using Domain.Entities;
using Microsoft.AspNetCore.Http;
using System;

namespace Services.Implementations.Exceptions;

public class InvalidRefreshTokenException : HttpExceptionBase
{
    private const string MESSAGE = "Неверный refresh токен.";

    public InvalidRefreshTokenException()
        : base(
              httpStatusCode: StatusCodes.Status401Unauthorized,
              message: string.Format(MESSAGE))
    {
    }
}