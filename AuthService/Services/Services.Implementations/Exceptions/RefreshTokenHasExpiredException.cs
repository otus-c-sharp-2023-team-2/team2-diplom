﻿using Domain.Entities;
using Microsoft.AspNetCore.Http;
using System;

namespace Services.Implementations.Exceptions;

public class RefreshTokenHasExpiredException : HttpExceptionBase
{
    private const string MESSAGE = "Refresh токен истек {0}.";

    public RefreshTokenHasExpiredException(DateTime expiration)
        : base(
              httpStatusCode: StatusCodes.Status401Unauthorized,
              message: string.Format(MESSAGE, expiration.ToString("S")))
    {
    }
}