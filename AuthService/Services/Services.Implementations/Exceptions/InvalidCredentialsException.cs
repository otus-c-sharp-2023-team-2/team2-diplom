﻿using Domain.Entities;
using Microsoft.AspNetCore.Http;

namespace Services.Implementations.Exceptions;

public class InvalidCredentialsException : HttpExceptionBase
{
    private const string MESSAGE = "Неверный логин или пароль.";

    public InvalidCredentialsException()
        : base(
              httpStatusCode: StatusCodes.Status401Unauthorized,
              message: string.Format(MESSAGE))
    {
    }
}
