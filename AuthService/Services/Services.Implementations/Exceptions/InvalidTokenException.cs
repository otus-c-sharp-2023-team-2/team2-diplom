﻿using Domain.Entities;
using Microsoft.AspNetCore.Http;
using System;

namespace Services.Implementations.Exceptions;

public class InvalidTokenException : HttpExceptionBase
{
    private const string MESSAGE = "Неверный Access токен.";

    public InvalidTokenException(Exception ex=null)
        : base(
              httpStatusCode: StatusCodes.Status401Unauthorized,
              message: string.Format(MESSAGE),
              inner: ex)
    {
    }
}