﻿using Domain.Entities;
using Microsoft.AspNetCore.Http;
using System;

namespace Services.Implementations.Exceptions;

public class RefreshTokenIsNotActiveException : HttpExceptionBase
{
    private const string MESSAGE = "Неверный refresh токен.";

    public RefreshTokenIsNotActiveException()
        : base(
              httpStatusCode: StatusCodes.Status401Unauthorized,
              message: string.Format(MESSAGE))
    {
    }
}