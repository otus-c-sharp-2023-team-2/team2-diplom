﻿using Domain.Entities;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Services.Contracts;
using Services.Implementations.Exceptions;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;

namespace Services.Abstractions;

public class JWTAuthTokenService : IAuthTokenService
{
    private readonly IOptions<JWTTokenSettings> _settings;
    private readonly RsaSecurityKey _rsaSecurityKey;

    public JWTAuthTokenService(IOptions<JWTTokenSettings> settings, RsaSecurityKey rsaSecurityKey)
    {
        _settings = settings;
        _rsaSecurityKey = rsaSecurityKey;
    }

    public string GenerateAccessToken(User user)
    {
        var signingCredentials = new SigningCredentials(
            key: _rsaSecurityKey,
            algorithm: SecurityAlgorithms.RsaSha256
        );

        
        var jwtHandler = new JwtSecurityTokenHandler();

        var jwt = jwtHandler.CreateJwtSecurityToken(
            issuer: _settings.Value.AccessTokenSettings.Issuer,
            audience: _settings.Value.AccessTokenSettings.Audience,
            subject: GetUserClaims(user),
            notBefore: DateTime.UtcNow,
            expires: DateTime.UtcNow.AddSeconds(_settings.Value.AccessTokenSettings.LifeTimeInSeconds),
            issuedAt: DateTime.UtcNow,
            signingCredentials: signingCredentials);

        var serializedJwt = jwtHandler.WriteToken(jwt);

        return serializedJwt;
    }

    private ClaimsIdentity GetUserClaims(User user)
    {
        var claimsIdentity = new ClaimsIdentity();

        // Add scope claim, which contains an array of scopes
        var scope = user.Claims?.FirstOrDefault(c => c.Type == "scope");
        if (scope != null) claimsIdentity.AddClaim(new System.Security.Claims.Claim("scope", string.Join(" ", scope.Value)));

        claimsIdentity.AddClaim(new System.Security.Claims.Claim(ClaimTypes.NameIdentifier, user.Id.ToString()));
        claimsIdentity.AddClaim(new System.Security.Claims.Claim(ClaimTypes.Name, user.Name));
        claimsIdentity.AddClaim(new System.Security.Claims.Claim(ClaimTypes.Email, user.Email));
        claimsIdentity.AddClaim(new System.Security.Claims.Claim(ClaimTypes.GivenName, user.Name));
        claimsIdentity.AddClaim(new System.Security.Claims.Claim(ClaimTypes.Surname, user.LastName));

		foreach (var c in user.Claims ?? Enumerable.Empty<CustomClaim>())
        {
            claimsIdentity.AddClaim(new System.Security.Claims.Claim(c.Type, c.Value));
        }

        return claimsIdentity;
    }

    
    public string GenerateIdToken(User user)
    {
        var signingCredentials = new SigningCredentials(
            key: _rsaSecurityKey,
            algorithm: SecurityAlgorithms.RsaSha256
        );

        var claimsIdentity = new ClaimsIdentity();

        claimsIdentity.AddClaim(new System.Security.Claims.Claim(ClaimTypes.NameIdentifier, user.Id.ToString()));
        claimsIdentity.AddClaim(new System.Security.Claims.Claim(ClaimTypes.Name, user.Name));
        claimsIdentity.AddClaim(new System.Security.Claims.Claim(ClaimTypes.Email, user.Email));
        claimsIdentity.AddClaim(new System.Security.Claims.Claim(ClaimTypes.GivenName, user.Name));
        claimsIdentity.AddClaim(new System.Security.Claims.Claim(ClaimTypes.Surname, user.LastName));

        // Add custom claims if any
        foreach (var c in user.Claims ?? Enumerable.Empty<CustomClaim>())
        {
            claimsIdentity.AddClaim(new System.Security.Claims.Claim(c.Type, c.Value));
        }

        var jwtHandler = new JwtSecurityTokenHandler();

        var jwt = jwtHandler.CreateJwtSecurityToken(
            issuer: _settings.Value.AccessTokenSettings.Issuer,
            audience: _settings.Value.AccessTokenSettings.Audience,
            subject: claimsIdentity,
            notBefore: DateTime.UtcNow,
            expires: DateTime.UtcNow.AddSeconds(_settings.Value.AccessTokenSettings.LifeTimeInSeconds),
            issuedAt: DateTime.UtcNow,
            signingCredentials: signingCredentials);

        var serializedJwt = jwtHandler.WriteToken(jwt);

        return serializedJwt;
    }
    
    public string GenerateRefreshToken()
    {
        var size = _settings.Value.RefreshTokenSettings.Length;
        var buffer = new byte[size];
        
        using var rng = RandomNumberGenerator.Create();
        
        rng.GetBytes(buffer);
        var refreshToken = Convert.ToBase64String(buffer);
        
        return refreshToken;
    }
    
    public int GetRefreshTokenLifetimeInMinutes()
    {
        return _settings.Value.RefreshTokenSettings.LifeTimeInMinutes;
    }
    
    public Guid GetUserIdFromToken(string token)
    {
        try
        {
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidateAudience = true,
                ValidateLifetime = false, // we may be trying to validate an expired token so it makes no sense checking for it's lifetime.
                ValidateIssuerSigningKey = true,
                ValidIssuer = _settings.Value.AccessTokenSettings.Issuer,
                ValidAudience = _settings.Value.AccessTokenSettings.Audience,
                IssuerSigningKey = _rsaSecurityKey,
                ClockSkew = TimeSpan.FromMinutes(0)
            };

            var jwtHandler = new JwtSecurityTokenHandler();
            var claims = jwtHandler.ValidateToken(token, tokenValidationParameters, out _);
            var userId = Guid.Parse(claims.FindFirst(ClaimTypes.NameIdentifier).Value);

            return userId;
        }
        catch (Exception ex)
        {
            throw new InvalidTokenException(ex);
        }
    }

    public bool IsTokenValid(string token, bool validateLifeTime)
    {
        var tokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuer = true,
            ValidateAudience = true,
            ValidateLifetime = validateLifeTime,
            ValidateIssuerSigningKey = true,
            ValidIssuer = _settings.Value.AccessTokenSettings.Issuer,
            ValidAudience = _settings.Value.AccessTokenSettings.Audience,
            IssuerSigningKey = _rsaSecurityKey,
            ClockSkew = TimeSpan.FromMinutes(0)
        };

        var jwtHandler = new JwtSecurityTokenHandler();
        try
        {
            jwtHandler.ValidateToken(token, tokenValidationParameters, out _);
            return true;
        }
        catch
        {
            return false;
        }
    }
}
