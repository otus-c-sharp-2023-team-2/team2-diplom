﻿using System;

namespace Services.Contracts.SignOut;

public class SignOutRequest : RequestBase
{
    public Guid UserId { get; set; }
}
