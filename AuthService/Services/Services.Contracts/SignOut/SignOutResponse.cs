﻿namespace Services.Contracts.SignOut;

public class SignOutResponse : ResponseBase
{
    public string Message { get; set; }
}
