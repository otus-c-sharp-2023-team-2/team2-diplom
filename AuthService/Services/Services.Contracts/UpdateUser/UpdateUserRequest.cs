﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Services.Contracts.CreateUser;

public class UpdateUserRequest : RequestBase
{
	[Required]
	public Guid UserId { get; set; }

	public bool? IsActive { get; set; }

	public string Email { get; set; }

    public string Password { get; set; }

    public string Name { get; set; }

    public string LastName { get; set; }

    public IEnumerable<Claim> Claims { get; set; }
}
