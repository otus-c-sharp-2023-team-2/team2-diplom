﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Services.Contracts.CreateUser;

public class CreateUserRequest : RequestBase
{
    [Required]
    public string Email { get; set; }

    [Required]
    public string Password { get; set; }

    public string Name { get; set; }

    public string LastName { get; set; }
}
