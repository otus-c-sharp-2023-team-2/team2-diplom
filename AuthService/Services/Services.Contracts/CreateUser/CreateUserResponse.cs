﻿using System;

namespace Services.Contracts.CreateUser;

public class CreateUserResponse : ResponseBase
{
    public Guid UserId { get; set; }
}
