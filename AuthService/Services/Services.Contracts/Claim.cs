﻿namespace Services.Contracts;

public class Claim
{
    public string Type { get; set; }
    public string Value { get; set; }
}
