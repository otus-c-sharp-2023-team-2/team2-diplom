﻿using Domain.Entities;
using System.Collections.Generic;

namespace Services.Contracts.CreateUser;

public class GetUsersRolesListResponse : ResponseBase
{
    public IEnumerable<User> Users { get; set; }
}
