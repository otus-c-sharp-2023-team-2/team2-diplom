﻿namespace Services.Contracts.Login
{
    public class LoginRequest : RequestBase
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
