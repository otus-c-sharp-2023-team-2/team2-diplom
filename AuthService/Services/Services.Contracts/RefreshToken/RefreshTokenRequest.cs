﻿namespace Services.Contracts.RefreshToken;

public class RefreshTokenRequest : RequestBase
{
    public string AccessToken { get; set; }
    public string RefreshToken { get; set; }
}
