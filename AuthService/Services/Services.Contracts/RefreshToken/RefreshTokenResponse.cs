﻿using System;

namespace Services.Contracts.RefreshToken;

public class RefreshTokenResponse : ResponseBase
{
    public string AccessToken { get; set; }
    public string RefreshToken { get; set; }
    public DateTime RefreshTokenExpirationDate { get; set; }
}
