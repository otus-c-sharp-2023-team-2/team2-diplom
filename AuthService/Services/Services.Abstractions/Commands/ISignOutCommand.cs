﻿using Services.Contracts.SignOut;

namespace Services.Abstractions.Commands;

public interface ISignOutCommand : ICommand<SignOutRequest, SignOutResponse>
{
}
