﻿using Services.Contracts.CreateUser;

namespace Services.Abstractions.Commands;

public interface IGetUsersCommand : ICommand<GetUsersRolesListRequest, GetUsersRolesListResponse>
{
}
