﻿using Services.Contracts.RefreshToken;

namespace Services.Abstractions.Commands;

public interface IRefreshTokenCommand : ICommand<RefreshTokenRequest, RefreshTokenResponse>
{
}
