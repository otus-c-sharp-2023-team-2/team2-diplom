﻿using Services.Contracts.CreateUser;

namespace Services.Abstractions.Commands;

public interface ICreateUserCommand : ICommand<CreateUserRequest, CreateUserResponse>
{
}
