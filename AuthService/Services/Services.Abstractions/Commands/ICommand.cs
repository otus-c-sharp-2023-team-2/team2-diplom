﻿using Services.Contracts;
using System.Threading.Tasks;

namespace Services.Abstractions.Commands;

public interface ICommand<R, S>
    where R : RequestBase
    where S : ResponseBase
{
    public Task<S> Execute(R request);
}
