﻿using Services.Contracts.Login;

namespace Services.Abstractions.Commands;

public interface ILoginCommand : ICommand<LoginRequest, LoginResponse>
{
}
