﻿using Services.Contracts.CreateUser;

namespace Services.Abstractions.Commands;

public interface IUpdateUserCommand : ICommand<UpdateUserRequest, UpdateUserResponse>
{
}
