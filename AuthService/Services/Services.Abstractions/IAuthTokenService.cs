﻿using Domain.Entities;
using System;

namespace Services.Abstractions;

public interface IAuthTokenService
{
    string GenerateIdToken(User user);
    string GenerateAccessToken(User user);
    string GenerateRefreshToken();
    Guid GetUserIdFromToken(string token);
    int GetRefreshTokenLifetimeInMinutes();
    bool IsTokenValid(string accessToken, bool validateLifeTime);
}
