﻿using Domain.Entities;

namespace Infrastructure.Repositories.Implementations;

internal static class UpdateHelper
{
    public static void UpdateRefreshtoken(this RefreshToken updating, RefreshToken data)
    {
        updating.Value = data.Value;
        updating.Active = data.Active;
        updating.ExpirationDate = data.ExpirationDate;
    }

	public static void UpdateUser(this User updating, User data)
    {
        if (updating is null)
            updating = new User();

        updating.Active = data.Active;
        updating.Email = data.Email;
        updating.Password = data.Password;
        updating.Salt = data.Salt;
        updating.Name = data.Name;
        updating.LastName = data.LastName;
        updating.CreationDate = data.CreationDate;
        updating.UpdateDate = data.UpdateDate;
    }
}
