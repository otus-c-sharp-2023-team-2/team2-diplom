﻿using Domain.Entities;
using Microsoft.AspNetCore.Http;
using System;
using System.Net.Mail;

namespace Infrastructure.Repositories.Implementations.Exceptions;

public class UserAlreadyExistException : HttpExceptionBase
{
    private const string MESSAGE = "Пользователь с таким email уже существует.";

    public UserAlreadyExistException(Guid id)
        : base(
              httpStatusCode: StatusCodes.Status409Conflict,
              message: string.Format(MESSAGE, id))
    {
    }
}
