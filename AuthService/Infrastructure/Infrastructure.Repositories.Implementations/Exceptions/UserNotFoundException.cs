﻿using Domain.Entities;
using Microsoft.AspNetCore.Http;
using System;

namespace Infrastructure.Repositories.Implementations.Exceptions;

public class UserNotFoundException : HttpExceptionBase
{
    private const string MESSAGE_FROM_ID = "Пользователь с Id {0} не найден.";

    private const string MESSAGE_FROM_EMAIL = "Пользователь с e-mail {0} не найден.";

    public UserNotFoundException(Guid id)
        : base(
              httpStatusCode: StatusCodes.Status404NotFound,
              message: string.Format(MESSAGE_FROM_ID, id))
    {
    }

    public UserNotFoundException(string email)
        : base(
              httpStatusCode: StatusCodes.Status404NotFound,
              message: string.Format(MESSAGE_FROM_EMAIL, email))
    {
    }
}
