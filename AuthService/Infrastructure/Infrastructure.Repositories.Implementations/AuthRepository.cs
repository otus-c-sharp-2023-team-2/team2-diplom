﻿using Domain.Entities;
using Infrastructure.EntityFramework;
using Infrastructure.Repositories.Implementations.Exceptions;
using Microsoft.EntityFrameworkCore;
using Services.Repositories.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure.Repositories.Implementations;

public class AuthRepository : IAuthRepository
{
    private readonly DbContext _dbContext;
    private readonly DbSet<User> _users;
    public AuthRepository(DatabaseContext dbContext)
    {
        _dbContext = dbContext;
        _users = _dbContext.Set<User>();
    }

    public async Task<User> GetUserById(Guid userId)
    {
        var result = await _users
            .Where(u => u.Id == userId)
            .Include(u => u.RefreshToken)
            .Include(u => u.Claims)
            .AsNoTracking()
            .FirstOrDefaultAsync();

        if (result == null)
            throw new UserNotFoundException(userId);

        return result;
    }

    public async Task<User> GetUserByEmail(string email)
    {
        var result = await _users
            .Where(u => u.Email == email)
            .Include(u => u.RefreshToken)
            .Include(u => u.Claims)
            .AsNoTracking()
            .FirstOrDefaultAsync();

        if (result == null)
            throw new UserNotFoundException(email);

        return result;
    }

    public async Task UpdateUser(User user)
    {
        var entity = await _users
            .Where(u => u.Id == user.Id)
            .Include(u => u.RefreshToken)
            .Include(u => u.Claims)
            .FirstOrDefaultAsync();

        if (entity == null)
            throw new UserNotFoundException(user.Id);

        user.UpdateDate = DateTime.UtcNow;

        if (entity.RefreshToken is not null)
        {
            entity.RefreshToken.UpdateRefreshtoken(user.RefreshToken);
        }
        else
        {
            entity.RefreshToken = user.RefreshToken;
        }

        if (user.Claims is not null)
        {
            entity.Claims = user.Claims;
        }

        entity.UpdateUser(user);

        await _dbContext.SaveChangesAsync();
    }

    public async Task CreateUser(User user)
    {
        var entity = await _users
            .Where(u => u.Id == user.Id || user.Email == u.Email)
            .AsNoTracking()
            .FirstOrDefaultAsync();

        if (entity is not null)
            throw new UserAlreadyExistException(user.Id);

        await _users.AddAsync(user);
        await _dbContext.SaveChangesAsync();
    }


    public async Task<IEnumerable<User>> GetUsersRolesList()
    {
        var entities = await _users
            .Include(u => u.Claims)
            .AsNoTracking()
            .ToListAsync();

        return entities;
    }
}
