﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Security.Claims;

namespace Infrastructure.EntityFramework.Extensions;

internal static class ModelBuilderExtensions
{
    /// <summary>
    /// Применить тестовые данные к БД.
    /// </summary>
    public static void Seed(this ModelBuilder modelBuilder)
    {

        modelBuilder.Entity<User>()
                    .HasData(
                        new User
                        {
                            Id = Guid.Parse("cad4244b-ffaf-48a0-b798-ea41b4cce215"),
                            Active = true,
                            Email = "tatyana.antipova@gmail.com",
                            Password = "Jt8YEhlRkcCE1PIAqoBVNnAQKnCLdGhRFQ0KzAUKP3s=", // string312
                            Salt = "cMsfyIdxUBau9kJBGvOpaynGQgg23Axwj8NUTTnxHkSb+vZbNa2mcLT2h1qChrq/Pg+u943lXklFm17KgMxvgA==",
                            Name = "Татьяна",
                            LastName = "Антипова",
                            RefreshToken = null
                        });

        modelBuilder.Entity<CustomClaim>()
					.HasData(
						new CustomClaim
						{
                            Id = Guid.NewGuid(),
							Type = ClaimTypes.Role,
							Value = nameof(Roles.Admin),
							UserId = Guid.Parse("cad4244b-ffaf-48a0-b798-ea41b4cce215")
						});
        /*-----------------------------------------------------------*/
        modelBuilder.Entity<User>()
                    .HasData(
                        new User
                        {
                            Id = Guid.Parse("103d165c-6062-4470-a318-09ca1a5350cc"),
                            Active = true,
                            Email = "testUser1@gmail.com",
                            Password = "As6znEgxxGKl/7JKchaE0u+6YPW1MJ/HS2Xu/gPfTS4=", // 12345
                            Salt = "3Qg6LxvcB+jmQIG2Vpd86X/+Ntb7PkCXyiO4qWOQjjwKsQnHdOTkGyi1onc8VaEMre2MQyZk/4l+eDQLdxuPNQ==",
                            Name = "Тестовый",
                            LastName = "Пользователь",
                            RefreshToken = null
                        });

        modelBuilder.Entity<CustomClaim>()
                    .HasData(
                        new CustomClaim
                        {
                            Id = Guid.NewGuid(),
                            Type = ClaimTypes.Role,
                            Value = nameof(Roles.None),
                            UserId = Guid.Parse("103d165c-6062-4470-a318-09ca1a5350cc")
                        });
        /*-----------------------------------------------------------*/
        modelBuilder.Entity<User>()
                    .HasData(
                        new User
                        {
                            Id = Guid.Parse("82f707bb-044e-48e7-af30-67db8bbbb00b"),
                            Active = true,
                            Email = "testUser2@gmail.com",
                            Password = "3F3IDbyrBNM3f/NkIN9p66bv4XrksTY7oVC4FrNGP2A=", // 12345
                            Salt = "AOW4W67fYVNoRNbcK7Spk34OPDesnsm8mTSrvMTr55iedmwdwnC2NYMhBkldyoNiTD0OofyoIQwjfGBSxDiiyg==",
                            Name = "Другой",
                            LastName = "Пользователь",
                            RefreshToken = null
                        });

        modelBuilder.Entity<CustomClaim>()
                    .HasData(
                        new CustomClaim
                        {
                            Id = Guid.NewGuid(),
                            Type = ClaimTypes.Role,
                            Value = nameof(Roles.Trainer),
                            UserId = Guid.Parse("82f707bb-044e-48e7-af30-67db8bbbb00b")
                        });
    }
}
