﻿using System;

namespace Domain.Entities;

public class RefreshToken
{
    public Guid Id { get; set; }
    public string Value { get; set; }
    public bool Active { get; set; }
    public DateTime ExpirationDate { get; set; }
    public Guid UserId { get; set; }
    public virtual User User { get; set; }
}
