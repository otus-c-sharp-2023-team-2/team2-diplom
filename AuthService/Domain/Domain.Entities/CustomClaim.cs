﻿using System;

namespace Domain.Entities;

public class CustomClaim
{
    public Guid Id { get; set; }
    public string Type { get; set; }
    public string Value { get; set; }
    public Guid UserId { get; set; }
    public virtual User User { get; set; }
}
