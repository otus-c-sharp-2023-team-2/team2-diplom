﻿using System;
using System.Net;

namespace Domain.Entities;

public class HttpExceptionBase : Exception
{
    private readonly int httpStatusCode;

    public HttpExceptionBase(int httpStatusCode)
    {
        this.httpStatusCode = httpStatusCode;
    }

    public HttpExceptionBase(HttpStatusCode httpStatusCode)
    {
        this.httpStatusCode = (int)httpStatusCode;
    }

    public HttpExceptionBase(int httpStatusCode, string message) : base(message)
    {
        this.httpStatusCode = httpStatusCode;
    }

    public HttpExceptionBase(HttpStatusCode httpStatusCode, string message) : base(message)
    {
        this.httpStatusCode = (int)httpStatusCode;
    }

    public HttpExceptionBase(int httpStatusCode, string message, Exception inner) : base(message, inner)
    {
        this.httpStatusCode = httpStatusCode;
    }

    public HttpExceptionBase(HttpStatusCode httpStatusCode, string message, Exception inner) : base(message, inner)
    {
        this.httpStatusCode = (int)httpStatusCode;
    }

    public int StatusCode { get { return this.httpStatusCode; } }
}
