﻿using System;
using System.Collections.Generic;

namespace Domain.Entities;

public class User
{
    public Guid Id { get; set; }
    public bool Active { get; set; }
    public string Email { get; set; }
    public string Password { get; set; }
    public string Salt { get; set; }
    public string Name { get; set; }
    public string LastName { get; set; }
    public virtual ICollection<CustomClaim> Claims { get; set; }
    public virtual RefreshToken RefreshToken { get; set; }
    public DateTime CreationDate { get; set; }
    public DateTime UpdateDate { get; set; }
}
