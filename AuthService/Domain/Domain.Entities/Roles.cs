﻿using System;

namespace Domain.Entities;

/// <summary>
/// Роли зарегистрированного пользователя.
/// </summary>
public enum Roles
{
	None,

	/// <summary>
	/// Посетитель.
	/// </summary>
	Visitor,

	/// <summary>
	/// Тренер.
	/// </summary>
	Trainer,

	/// <summary>
	/// Администратор.
	/// </summary>
	Admin,
}
