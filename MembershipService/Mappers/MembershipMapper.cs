﻿using Domain;
using Entities;
using Model;


namespace Mappers
{

    // из Domein to Repository
    public static class MembershipMapper
    {
        public static MembershipEntity ToEntity(this Membership membership)
        {
            return new MembershipEntity
            {
                UserId = membership.UserId,
                Type = membership.Type,
                Duration = membership.Duration,
                Price = membership.Price,
                StartDate = membership.StartDate
            };
        }

        //из Model to Domain
        public static Membership ToDomain(this MembershipModel membershipModel)
        {
            return new Membership
            {
                UserId = membershipModel.UserId,
                Type = membershipModel.Type,
                Duration = membershipModel.Duration,
                Price = membershipModel.Price,
                StartDate = membershipModel.StartDate
            };
        }


        //из Domain to Model
        public static MembershipModel ToModel(this Membership membership)
        {
            return new MembershipModel
            {
                UserId = membership.UserId,
                Type = membership.Type,
                Duration = membership.Duration,
                Price = membership.Price,
                StartDate = membership.StartDate
            };
        }

        //из Entity to Domain
        public static Membership EntityToDomain(this MembershipEntity membershipEntity)
        {
            return new Membership
            {
                UserId = membershipEntity.UserId,
                Type = membershipEntity.Type,
                Duration = membershipEntity.Duration,
                Price = membershipEntity.Price,
                StartDate = membershipEntity.StartDate
            };
        }



    }
}
