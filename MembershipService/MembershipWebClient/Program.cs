﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Json;



// Принимает с консоли ID "Клиента"

 Console.WriteLine("Введите Id");
 _ = Guid.TryParse(Console.ReadLine(), out Guid Id);

  using (var client = new HttpClient())
    {

     // запрашивает его с сервера

      var response = client.GetAsync("https://localhost:7112/Membership/GetById/" + Id).Result;

      var result = response.Content.ReadAsStringAsync().Result;


      // и отображает его данные по пользователю;
      Console.WriteLine("Абонемент с запрошенным ID");
      Console.WriteLine(response.StatusCode + "   " + result);

      };

Console.ReadKey();

// 25092023