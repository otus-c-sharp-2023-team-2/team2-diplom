﻿using Entities.Abstracts;

namespace Entities
{
    public class MembershipEntity : BaseEntity
    {
        public Guid UserId { get; set; }
        public string Type { get; set; }
        public int Duration { get; set; }
        public double Price { get; set; }
        public DateTime StartDate { get; set; }
    }
}
