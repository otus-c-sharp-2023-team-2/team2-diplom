using Mappers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis;
using Model;
using Services.Abstract;


namespace MembershipApi.Controllers;

[ApiController]
[Route("[controller]")]
public class MembershipController : ControllerBase
{
    private readonly IMembershipService _membershipService;

    public MembershipController (IMembershipService membershipService)
    {
        _membershipService = membershipService;
    }

    [HttpPost]
    public void AddMembeship([FromBody] MembershipModel value)
    {

        _membershipService.AddMembership(value.ToDomain());
        Console.WriteLine("Point 1 Controller   " +  value.Type); //for debug
    }


    [HttpGet("GetById/{id:Guid}")]
    public MembershipModel GetByID([FromRoute] Guid id)
    {
        return (_membershipService.GetMembershipById(id)).ToModel();
    }

    [HttpGet("{id}")]
    public async Task<IEnumerable<MembershipModel>> GetByUser([FromRoute] Guid id)
    {
        var result = await _membershipService.GetMembershipByUserAsync(id);
        return result.Select(x=>x.ToModel());
    }

    [HttpDelete("DeleteById/{id:int}")]
    public void Delete ([FromRoute] Guid id)
    {
        _membershipService.DeleteMembershipById(id);

    }

    [HttpGet("all")]
    public List<MembershipModel> GetAll()
    {
        var values = new List<MembershipModel>();
        {
            foreach (var tt in _membershipService.GetAll())
                values.Add(tt.ToModel());
        }
        
        return values;
    }
}