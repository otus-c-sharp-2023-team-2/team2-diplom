using Data;
using Data.Repositories;
using Data.Repositories.Abstract;
using Microsoft.EntityFrameworkCore;
using Services;
using Services.Abstract;


//  11.07.2023 ������� ��������� �������� � ������ ���� ������

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddDbContext<MembershipEntityContext>(options =>
    options.UseNpgsql(builder.Configuration.GetConnectionString("MembershipEntityContext") ?? throw new InvalidOperationException("Connection string 'MembershipEntityContext' not found.")));

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddScoped<IMembershipService, MembershipService>();
builder.Services.AddScoped<IMembershipsRepository, MembershipsRepository>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI(c=>
    {
        c.SwaggerEndpoint("/swagger/v1/swagger.json", "MembershipService");
        c.RoutePrefix = string.Empty;
        });
}

app.UseAuthorization();

app.MapControllers();

app.Run();
