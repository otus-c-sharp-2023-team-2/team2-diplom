﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class MembershipModel
    {
        public Guid UserId { get; set; }
        public string Type { get; set; }
        public int Duration { get; set; }
        public double Price { get; set; }
        public DateTime StartDate { get; set; }
    }
}
