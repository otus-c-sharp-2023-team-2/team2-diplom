﻿using Entities;

namespace Data.Repositories.Abstract
{
    public interface IMembershipsRepository
    {
        void AddMembership(MembershipEntity membership);
        void UpdateMembership(MembershipEntity membership);
        MembershipEntity GetMembershipById(Guid id);
        Task<IEnumerable<MembershipEntity>> GetMembershipByUserAsync(Guid id);
        void DeleteMembershipById(Guid iD);
        List<MembershipEntity> GetAll();
    }
}
