﻿using Data.Repositories.Abstract;
using Entities;
using Entities.Abstracts;
using Microsoft.EntityFrameworkCore.Scaffolding.Metadata;
using System;
using Microsoft.EntityFrameworkCore;


namespace Data.Repositories
{
    //CRUD Repository
    public class MembershipsRepository : IMembershipsRepository
    {
        //? private static List<MembershipEntity> _members = new List<MembershipEntity>();

        public MembershipEntityContext _context;

        public MembershipsRepository (MembershipEntityContext context)
        {
            _context = context;
        }


        public void AddMembership(MembershipEntity membership) 
        {
            Console.WriteLine("Point 3 Repository   " +  membership.Type);
            _context.Add(membership);
            _context.SaveChanges();
        }
              
        
        
        public void UpdateMembership(MembershipEntity membership) 
        {
            throw new NotImplementedException();
            // var membershipUpdate = _context.MembershipEntity.Where(x => x.);
            //// membershipUpdate.
            //      _context.SaveChanges();
        }
       
        
        public MembershipEntity GetMembershipById(Guid id) 
        {
            return _context.Find<MembershipEntity>(id);
        }

        public async Task<IEnumerable<MembershipEntity>> GetMembershipByUserAsync(Guid id)
        {
            return await _context.MembershipEntity
                .Where(x => x.UserId == id)
                .AsNoTracking()
                .ToListAsync();
        }

        public void DeleteMembershipById(Guid iD) 
        {        
        var forDeleteEntity = _context.Find<MembershipEntity> (iD);
            if (forDeleteEntity != null) { }

        _context.Remove(forDeleteEntity);
        _context.SaveChanges();
        }

        public List<MembershipEntity> GetAll()
        {

            var membersip = new List<MembershipEntity>() ;

            var mm = _context.MembershipEntity.Where(x => x.UserId != null);


            foreach (var yy in mm)
            {
                membersip.Add(yy);

            }
            return membersip;

        }


    }
}