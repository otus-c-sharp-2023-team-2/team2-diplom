﻿using Microsoft.EntityFrameworkCore;
using Entities;

namespace Data
{
    public class MembershipEntityContext : DbContext
    {
        public MembershipEntityContext (DbContextOptions<MembershipEntityContext> options) : base(options)
        {
            AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
            Database.EnsureCreated();
        }

        public DbSet<MembershipEntity> MembershipEntity { get; set; } = default!;

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<MembershipEntity>()
                .HasData(new MembershipEntity()
                {
                    Id = Guid.NewGuid(),
                    UserId = new Guid("103d165c-6062-4470-a318-09ca1a5350cc"),
                    Type = "Один зал",
                    Duration = 3,
                    Price = 1500,
                    StartDate = new DateTime(2023, 4, 1)
                });

            modelBuilder.Entity<MembershipEntity>()
                .HasData(new MembershipEntity()
                {
                    Id = Guid.NewGuid(),
                    UserId = new Guid("103d165c-6062-4470-a318-09ca1a5350cc"),
                    Type = "Все залы",
                    Duration = 3,
                    Price = 1500,
                    StartDate = new DateTime(2023, 9, 15)
                });
        }
    }
}
