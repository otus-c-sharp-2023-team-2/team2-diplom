﻿using Data.Repositories.Abstract;
using Services.Abstract;
using System;
using Domain;
using Mappers;



namespace Services

{
    public class MembershipService : IMembershipService
    {
        private readonly IMembershipsRepository _membershipsRepository;
        public MembershipService(IMembershipsRepository membershipsRepository)
        {
            _membershipsRepository = membershipsRepository;
        }

        public void AddMembership(Membership membership)

        {
            Console.WriteLine("Point 2 Services   " + membership.Type);
            _membershipsRepository.AddMembership(membership.ToEntity());
        }
        public void UpdateMembership(Membership membership) { throw new NotImplementedException(); }
        public Membership GetMembershipById(Guid id) 
        
        { 
            return (_membershipsRepository.GetMembershipById(id)).EntityToDomain();

        }

        public async Task<IEnumerable<Membership>> GetMembershipByUserAsync(Guid id)
        {
            var result = await _membershipsRepository.GetMembershipByUserAsync(id);
            return result.Select(x=> x.EntityToDomain());
        }

        public void DeleteMembershipById(Guid id) 
        {
            _membershipsRepository.DeleteMembershipById(id);        
        }

        public List <Membership> GetAll()
        {
            var membership = new List<Membership>();

            foreach (var tt in _membershipsRepository.GetAll())
            {
                membership.Add(tt.EntityToDomain());
            }

            return membership;
        }

        // 18092023

       
    }
}
