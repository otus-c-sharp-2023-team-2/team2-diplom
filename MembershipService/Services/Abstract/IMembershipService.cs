﻿using Domain;
using Entities;

namespace Services.Abstract
{
    public interface IMembershipService
    {
        void AddMembership(Membership membership);
        void UpdateMembership(Membership membership);
        Membership GetMembershipById(Guid id);
        Task<IEnumerable<Membership>> GetMembershipByUserAsync(Guid id);
        void DeleteMembershipById(Guid iD);

        List<Membership> GetAll();

    }
}
