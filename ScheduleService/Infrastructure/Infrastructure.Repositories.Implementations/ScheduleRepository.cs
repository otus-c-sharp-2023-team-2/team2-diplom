﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Services.Repositories.Abstractions;
using Domain.Entities;
using Infrastructure.EntityFramework;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories.Implementations
{
    public class ScheduleRepository : Repository<Schedule, Guid>, IScheduleRepository
    {
        public ScheduleRepository(DatabaseContext context): base(context)
        {
        }
        
        //public async Task<Schedule> GetById(int id)
        //{
        //    return await GetAsync(id);
        //}

        //public async Task<int> Create(Schedule schedule)
        //{
        //    var res = await base.AddAsync(schedule);
        //    return res.Id;
        //}

        public async Task<List<Schedule>> GetSchedulesByGymAndDate(Guid gym) //todo enable date
        {
            return GetAll().Where(s => s.Gym == gym).ToList();
        }

        public async Task<List<Schedule>> GetSchedulesByCustomer(Guid customer)
        {
            return GetAll().Where(s => s.Customer == customer).ToList();
        }
    }
}
