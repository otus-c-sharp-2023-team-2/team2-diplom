using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Services.Contracts;

namespace Services.Abstractions
{ 
    public interface IScheduleService
    { 
        Task<ScheduleDto> GetById(Guid id);

        Task<Guid> Create(ScheduleDto schedule);

        Task<List<ScheduleDto>> GetSchedulesByGymAndDate(Guid gym);

        Task<List<ScheduleDto>> GetSchedulesByCustomer(Guid customer);
    }
}