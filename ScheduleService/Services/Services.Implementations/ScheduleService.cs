﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Services.Repositories.Abstractions;
using Services.Abstractions;
using AutoMapper;
using CommonNamespace;
using Domain.Entities;
using MassTransit;
using MassTransit.Initializers;
using Services.Contracts;

namespace Services.Implementations
{ 
    public class ScheduleService : IScheduleService
    {
        private readonly IMapper _mapper;
        private readonly IScheduleRepository _scheduleRepository;
        private readonly IBusControl _busControl;

        public ScheduleService(
            IMapper mapper,
            IScheduleRepository scheduleRepository)
            //IBusControl busControl)
        {
            _mapper = mapper;
            _scheduleRepository = scheduleRepository;
            //_busControl = busControl;
        }

        public async Task<ScheduleDto> GetById(Guid id)
        {
            var schedule = await _scheduleRepository.GetAsync(id);
            return _mapper.Map<Schedule, ScheduleDto>(schedule);
        }

        public async Task<Guid> Create(ScheduleDto schedule)
        {
            var _sched = _mapper.Map<ScheduleDto, Schedule>(schedule);
            var dbRes = await _scheduleRepository.AddAsync(_sched);
            await _scheduleRepository.SaveChangesAsync();
            return dbRes.Id;
        }

        public async Task<List<ScheduleDto>> GetSchedulesByGymAndDate(Guid gym)
        {
            var schedules = await _scheduleRepository.GetSchedulesByGymAndDate(gym);
            return schedules.Select(_mapper.Map<Schedule, ScheduleDto>).ToList();
        }

        public async Task<List<ScheduleDto>> GetSchedulesByCustomer(Guid customer)
        {
            var schedules = await _scheduleRepository.GetSchedulesByCustomer(customer);
            return schedules.Select(_mapper.Map<Schedule, ScheduleDto>).ToList();
        }
    }
}