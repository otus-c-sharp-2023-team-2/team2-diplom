using AutoMapper;
using Domain.Entities;
using Services.Contracts;

namespace Services.Implementations.Mapping
{
    public class ScheduleMappingsProfile : Profile
    {
        public ScheduleMappingsProfile()
        {
            CreateMap<Schedule, ScheduleDto>();

            CreateMap<ScheduleDto, Schedule>().ForMember(x => x.Id, opt => opt.Ignore());
        }
    }
}
