﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Entities;

namespace Services.Repositories.Abstractions
{
    public interface IScheduleRepository : IRepository<Schedule, Guid>
    {
        Task<List<Schedule>> GetSchedulesByGymAndDate(Guid gym);

        Task<List<Schedule>> GetSchedulesByCustomer(Guid customer);
    }
}
