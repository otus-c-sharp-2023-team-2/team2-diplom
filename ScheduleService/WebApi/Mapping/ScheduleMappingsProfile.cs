using AutoMapper;
using Services.Contracts;
using WebApi.Models;

namespace WebApi.Mapping
{ 
    public class ScheduleMappingsProfile : Profile
    {
        public ScheduleMappingsProfile()
        {
            CreateMap<ScheduleDto, ScheduleModel>();

            CreateMap<ScheduleModel, ScheduleDto>();
        }
    }
}
