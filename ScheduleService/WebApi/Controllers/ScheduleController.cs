﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Services.Abstractions;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Services.Contracts;
using WebApi.Models;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ScheduleController : ControllerBase
    {
        private IScheduleService _service;
        //private readonly ILogger<ScheduleController> _logger;
        private IMapper _mapper;

        public ScheduleController(IScheduleService service,
            //ILogger<ScheduleController> logger,
            IMapper mapper)
        {
            _service = service;
            //_logger = logger;
            _mapper = mapper;
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(ScheduleModel), 200)]
        public async Task<IActionResult> Get(Guid id)
        {
            var scheduleDto = await _service.GetById(id);
            if (scheduleDto == null)
                return NotFound();
            return Ok(_mapper.Map<ScheduleDto, ScheduleModel>(scheduleDto));
        }

        [HttpGet("Gym/{id}")]
        [ProducesResponseType(typeof(List<ScheduleModel>), 200)]
        public async Task<IActionResult> GetListByGym(Guid id) //todo add date
        {
            var scheduleDto = await _service.GetSchedulesByGymAndDate(id);
            if (scheduleDto == null || !scheduleDto.Any())
                return NotFound();
            return Ok(scheduleDto.Select(_mapper.Map<ScheduleDto, ScheduleModel>).ToList());
        }

        [HttpGet("Customer/{id}")]
        [ProducesResponseType(typeof(List<ScheduleModel>), 200)]
        public async Task<IActionResult> GetListByCustomer(Guid id)
        {
            var scheduleDto = await _service.GetSchedulesByCustomer(id);
            if (scheduleDto == null || !scheduleDto.Any())
                return NotFound();
            return Ok(scheduleDto.Select(_mapper.Map<ScheduleDto, ScheduleModel>).ToList());
        }

        [HttpPost]
        [ProducesResponseType(typeof(Guid), 200)]
        public async Task<IActionResult> Post(ScheduleModel input)
        {
            var schedule = _mapper.Map<ScheduleModel, ScheduleDto>(input);
            var scheduleId = await _service.Create(schedule);
            return Ok(scheduleId);
        }
    }
}