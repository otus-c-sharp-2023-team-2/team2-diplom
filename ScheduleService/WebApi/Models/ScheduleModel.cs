﻿using System;

namespace WebApi.Models
{
    public class ScheduleModel
    {
        public DateTime StartTime { get; set; }

        public DateTime EndTime { get; set; }

        public Guid Trainer { get; set; }

        public Guid Gym { get; set; }

        public Guid Customer { get; set; }
    }
}