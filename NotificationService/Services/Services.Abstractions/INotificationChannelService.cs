﻿using Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Abstractions
{
    /// <summary>
    /// Сервис работы с каналами уведомлений
    /// </summary>
    public interface INotificationChannelService
    {
        Task<NotificationChannelDto> GetById(Guid id);
        Task<List<NotificationChannelDto>> GetListByUserId(Guid id);
        Task<Guid> Create(NotificationChannelDto notificationDto);
        Task Update(NotificationChannelDto notificationDto);
        Task Delete(Guid id);
        Task DeleteByUserId(Guid id);
    }
}
