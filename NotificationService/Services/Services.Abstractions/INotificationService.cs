﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Services.Contracts;

namespace Services.Abstractions
{
    /// <summary>
    /// Сервис работы с уведомлениями
    /// </summary>
    public interface INotificationService
    {
        Task<NotificationDto> GetNotificationById(Guid id);
        Task<Guid> Create(NotificationDto notificationDto);
    }
}
