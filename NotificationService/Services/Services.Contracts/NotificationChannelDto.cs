﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Contracts
{
    public class NotificationChannelDto
    {
        /// <summary>
        /// ИД
        /// </summary>
        public Guid Id { get; init; }
        /// <summary>
        /// ИД пользователя
        /// </summary>
        public Guid UserId { get; init; }
        /// <summary>
        /// Способ уведомлений (почта/телеграм/СМС)
        /// </summary>
        public int ChannelType { get; init; }
        public string Address { get; init; }
    }
}
