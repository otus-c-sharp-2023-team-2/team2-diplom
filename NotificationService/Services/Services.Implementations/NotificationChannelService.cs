﻿using AutoMapper;
using Domain.Entities;
using Services.Abstractions;
using Services.Contracts;
using Services.Repositories.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Implementations
{
    /// <summary>
    /// Сервис работы с каналами уведомлений
    /// </summary>
    public class NotificationChannelService : INotificationChannelService
    {
        private readonly INotificationChannelRepository _notificationChannelRepository;
        private readonly IMapper _mapper;
        public NotificationChannelService(INotificationChannelRepository notificationChannelRepository, IMapper mapper)
        {
            _notificationChannelRepository = notificationChannelRepository;
            _mapper = mapper;
        }
        /// <summary>
        /// Получить
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<NotificationChannelDto> GetById(Guid id)
        {
            var _notificationChannel = await _notificationChannelRepository.GetAsync(id);
            if (_notificationChannel == null)
            {
                return null;
            }
            NotificationChannelDto _notificationChannelDto = new()
            {
                Id = id,
                UserId = _notificationChannel.UserId,
                ChannelType = _notificationChannel.ChannelType,
                Address=_notificationChannel.Address
            };
            return _notificationChannelDto;
        }
        /// <summary>
        /// получить все каналы уведомления для пользователя
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<List<NotificationChannelDto>> GetListByUserId(Guid id)
        {
            var _notificationChanList = await _notificationChannelRepository.GetByUserAsync(id);
            List<NotificationChannelDto> _res = new List<NotificationChannelDto>();
            foreach (var notificationChan in _notificationChanList)
            {
                _res.Add(new NotificationChannelDto { 
                    Id = notificationChan.Id, 
                    UserId = notificationChan.UserId,
                    ChannelType = notificationChan.ChannelType,
                    Address = notificationChan.Address
                });
            }
            return _res;
        }
        /// <summary>
        /// Создать
        /// </summary>
        /// <param name="notificationDto"></param>
        /// <returns></returns>
        public async Task<Guid> Create(NotificationChannelDto notificationDto)
        {
            NotificationChannel _notificationChannel = new()
            {
                Id=notificationDto.Id,
                UserId = notificationDto.UserId,
                ChannelType = notificationDto.ChannelType,
                Address=notificationDto.Address
            };
            var res = await _notificationChannelRepository.CreateAsync(_notificationChannel);
            await _notificationChannelRepository.SaveChangesAsync();
            return res.Id;
        }
        /// <summary>
        /// Удалить по ID канала уведомлений
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task Delete(Guid id)
        {
            _ = await _notificationChannelRepository.DeleteAsync(id);
            await _notificationChannelRepository.SaveChangesAsync();
        }
        /// <summary>
        /// Удалить все каналы по ID пользователя
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task DeleteByUserId(Guid id)
        {
            _ = await _notificationChannelRepository.DeleteAsyncByUserID(id);
            await _notificationChannelRepository.SaveChangesAsync();
        }
        /// <summary>
        /// Изменить канал уведомлений
        /// </summary>
        /// <param name="notchanDto"></param>
        /// <returns></returns>
        public async Task Update(NotificationChannelDto notchanDto)
        {
            //var entity = _mapper.Map<NotificationChannelDto, NotificationChannel>(notchanDto);
            NotificationChannel _notificationChannel = new()
            {
                Id = notchanDto.Id,
                UserId = notchanDto.UserId,
                ChannelType = notchanDto.ChannelType,
                Address=notchanDto.Address
            };
            _notificationChannelRepository.Update(_notificationChannel);
            await _notificationChannelRepository.SaveChangesAsync();
        }
    }
}
