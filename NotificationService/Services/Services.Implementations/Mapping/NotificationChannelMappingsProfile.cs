﻿using AutoMapper;
using Domain.Entities;
using Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Implementations.Mapping
{
    public class NotificationChannelMappingsProfile : Profile
    {
        public NotificationChannelMappingsProfile() 
        {
            CreateMap<NotificationChannel, NotificationChannelDto>();

            CreateMap<NotificationChannelDto, NotificationChannel>();
        }
    }
}
