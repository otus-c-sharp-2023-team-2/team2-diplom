﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Repositories.Abstractions
{
    public interface INotificationChannelRepository
    {
        Task<NotificationChannel> GetAsync(Guid id);
        Task<List<NotificationChannel>> GetByUserAsync(Guid userId);
        void Update(NotificationChannel notificationChannel);
        Task<bool> DeleteAsync(Guid id);
        Task<bool> DeleteAsyncByUserID(Guid id);
        Task<NotificationChannel> CreateAsync(NotificationChannel notificationChannel);
        Task SaveChangesAsync();
    }
}
