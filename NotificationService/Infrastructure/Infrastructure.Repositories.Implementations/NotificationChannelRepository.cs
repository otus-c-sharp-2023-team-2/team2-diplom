﻿using Domain.Entities;
using Infrastructure.EntityFramework;
using Microsoft.EntityFrameworkCore;
using Services.Repositories.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories.Implementations
{
    public class NotificationChannelRepository : INotificationChannelRepository
    {
        public readonly DbContext Context;
        private readonly DbSet<NotificationChannel> _notificationChannelSet;
        public NotificationChannelRepository(DatabaseContext context)
        {
            Context = context;
            _notificationChannelSet = Context.Set<NotificationChannel>();
        }
        public async Task<NotificationChannel> GetAsync(Guid id)
        {
            return await _notificationChannelSet.FindAsync(id);
        }
        public async Task<List<NotificationChannel>> GetByUserAsync(Guid userId)
        {
            return await _notificationChannelSet.Where(p => p.UserId == userId).ToListAsync();
        }
        public async Task<NotificationChannel> CreateAsync(NotificationChannel _notificationChannel)
        {
            return (await _notificationChannelSet.AddAsync(_notificationChannel)).Entity;
        }
        public async Task SaveChangesAsync()
        {
            await Context.SaveChangesAsync();
        }
        /// <summary>
        /// удалить все каналы уведомлений по ИД пользователя
        /// </summary>
        /// <param name="id"> GUID пользователя</param>
        /// <returns></returns>
        public async Task<bool> DeleteAsyncByUserID(Guid id)
        {
            var _res = await _notificationChannelSet.Where( p => p.UserId == id).ToListAsync();
            foreach (var channel in _res) 
            {
                _notificationChannelSet.Remove(channel);
            }
            return true;
        }
        /// <summary>
        /// удалить все каналы уведомления по ИД канала уведомления
        /// </summary>
        /// <param name="id"> GUID пользователя</param>
        /// <returns></returns>
        public async Task<bool> DeleteAsync(Guid id)
        {
            var _res = await _notificationChannelSet.FindAsync(id);
            if (_res != null)
            {
                _notificationChannelSet.Remove(_res);
                return true;
            }
            return true;
        }
        public void Update(NotificationChannel _notificationChannel)
        {
            //return _notificationChannelSet.Update(_notificationChannel).Entity;
            _notificationChannelSet.Update(_notificationChannel);
        }
    }
}
