﻿using System;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Infrastructure.EntityFramework.Extensions;
using Npgsql;

namespace Infrastructure.EntityFramework
{
    /// <summary>
    /// Контекст
    /// </summary>
    public class DatabaseContext : DbContext
    {      
        
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {
            //Database.EnsureDeletedAsync().Wait();
            //Database.EnsureCreated();
        }
        /// <summary>
        /// уведомления
        /// </summary>
        public DbSet<Notification> Notifications { get; set; }
        /// <summary>
        /// шаблоны уведомлений
        /// </summary>
        public DbSet<NotificationTemplate> NotificationTemplates { get; set; }
        /// <summary>
        /// Каналы уведомлений
        /// </summary>
        public DbSet<NotificationChannel> NotificationChannels { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.UseIdentityColumns();

            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Notification>().HasKey(x => x.Id);

            modelBuilder.Entity<NotificationChannel>().HasKey(x => x.Id);

            modelBuilder.Entity<NotificationTemplate>().HasKey(x => x.Id);

            modelBuilder.Seed();
        }
    }
}