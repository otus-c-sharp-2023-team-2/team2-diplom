﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    /// <summary>
    /// шаблон уведомления
    /// </summary>
    public class NotificationTemplate : IEntity<Guid>
    {
        public Guid Id { get; init; }
        public string Name { get; init; }
        public string Body { get; init; }
        public Guid UserId { get; init; }
    }
}
