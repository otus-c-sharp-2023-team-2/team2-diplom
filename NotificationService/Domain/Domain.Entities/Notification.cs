﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    /// <summary>
    /// Notification
    /// </summary>
    public class Notification : IEntity<Guid>
    {
        /// <summary>
        /// ИД
        /// </summary>
        public Guid Id { get; init; }
        /// <summary>
        /// Текст сообщения
        /// </summary>
        [Required]
        public string Body { get; init; }
        /// <summary>
        /// дата получения
        /// </summary>
        [Required]
        public DateTime SendingDateTime { get; init; }
        /// <summary>
        /// ИД пользователя
        /// </summary>
        [Required]
        public Guid UserID { get; init; }
        /// <summary>
        /// address
        /// </summary>
        public string Address { get; init; }
    }
}
