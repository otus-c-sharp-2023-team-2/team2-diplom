﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace Domain.Entities
{
    /// <summary>
    /// Способы уведомления
    /// </summary>
    public class NotificationChannel : IEntity<Guid>
    {
        /// <summary>
        /// ИД
        /// </summary>
        public Guid Id { get; init; }
        /// <summary>
        /// ИД пользователя
        /// </summary>
        public Guid UserId { get; init; }
        /// <summary>
        /// Тип уведомлений (почта/телеграм/СМС)
        /// </summary>
        public int ChannelType {get; init; }
        /// <summary>
        /// адрес почты или телефон или токен ТГ/дискорд
        /// </summary>
        public string Address { get; init; }

    }
}
