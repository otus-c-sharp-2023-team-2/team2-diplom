﻿using System.Threading;
using System.Threading.Tasks;
using WebApi.Models;

namespace WebApi.MessageMaster
{
    public interface IEmailMaster
    {
        public Task SendAsync(MailDataModel mailData, CancellationToken ct = default);
        public void Send(MailDataModel mailData, CancellationToken ct = default);

    }
}
