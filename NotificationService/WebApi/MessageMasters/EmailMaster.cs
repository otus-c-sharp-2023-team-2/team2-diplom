﻿using MailKit.Security;
using MailKit.Net.Smtp;
using Microsoft.Extensions.Options;
using MimeKit;
using Org.BouncyCastle.Asn1.Pkcs;
using System;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using WebApi.Models;
using System.Net.Sockets;
using MassTransit;
using MimeKit.Text;
using static Org.BouncyCastle.Math.EC.ECCurve;
using MailKit;
using Castle.Core.Logging;
using Microsoft.Extensions.Logging;
using System.Collections.Concurrent;
using WebApi.MessageMaster.Models;

namespace WebApi.MessageMaster
{
    /// <summary>
    /// Мастер отправки сообщений
    /// </summary>
    public class EmailMaster : IEmailMaster, IAsyncDisposable
    {        
        private SmtpClient _smtpClient;        
        private bool _disposed;
        private readonly Microsoft.Extensions.Logging.ILogger<EmailMaster> _logger;
        private readonly MailSettings _settings;
        private ConcurrentQueue<MimeMessage> _messagesToSend;
        private System.Threading.Timer _timerSend;
        private System.Threading.TimerCallback _timerSendCallBack;
        private static object _locker = new object();
        private bool _killAll;
        private AutoResetEvent _killTimerSignal;
        /// <summary>
        /// мастер, отвечающий за отправку сообщений. Одиночка. 
        /// </summary>
        /// <param name="settings"></param>
        /// <param name="logger"></param>
        public EmailMaster(IOptions<MailSettings> settings, Microsoft.Extensions.Logging.ILogger<EmailMaster> logger) 
        {
            _settings = settings.Value;
            _logger = logger;
            _killAll = false;
            _smtpClient = new SmtpClient();
            _messagesToSend = new ConcurrentQueue<MimeMessage> { };
            _logger.LogInformation("MessageMaster was run");
            _timerSendCallBack = new TimerCallback(SendTimerCallBack);
            int _sendInterval = 1000;
            if (_settings.SendInterval>10)
            {
                _sendInterval = _settings.SendInterval;
            }            
            _timerSend = new System.Threading.Timer(_timerSendCallBack, null, 100, _sendInterval);
        }       
        public async ValueTask DisposeAsync()
        {
            if (_disposed) return;
            _disposed = true;
            StopSending();

            if (_smtpClient.IsConnected)
            {
                //Есть вероятность эксепшена, тогда часть соединений не закроется никогда
                await _smtpClient.DisconnectAsync(true);
            }
            _smtpClient.Dispose();
            _logger.LogInformation("message master asyncdispose");
            GC.SuppressFinalize(this); // Говорим GC не вызывать финализатор
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void ThrowIfDisposed()
        {
            if (_disposed)
            {
                throw new ObjectDisposedException(nameof(_smtpClient));
            }
        }
        /// <summary>
        /// тик таймера для отправки сообщений по-одному
        /// </summary>
        /// <param name="state"></param>
        private void SendTimerCallBack(object state)
        {
            //проверяем закончился ли предыдущий тик таймера
            if (Monitor.TryEnter(_locker))
            {
                try
                {
                    //нужно ли останалвиваться?
                    if (!_killAll)
                    {
                        //шлем сообщения, если они есть
                        SendOneMessage();
                    }
                    else
                    {
                        //даем команду, что тик таймера закончился, занчит можно убивать таймер
                        _killTimerSignal.Set();
                    }
                }
                catch (Exception e)
                {
                    _logger.LogError($"Error in SendTimerTick {e}");
                }
                finally
                {
                    Monitor.Exit(_locker);
                }
            }
        }
        /// <summary>
        /// отправка 1 сообщения из очереди
        /// </summary>
        /// <exception cref="ConnectionException"></exception>
        private void SendOneMessage()
        {
            if (_messagesToSend.TryDequeue(out var message))
            {
                try
                {
                    EnsureConnectedAndAuthenticated();
                    _smtpClient.Send(message);
                    _smtpClient.Disconnect(true);
                }
                catch (Exception e) when (e is SmtpCommandException or SslHandshakeException or SocketException)
                {
                    throw new ConnectionException(e.Message, innerException: e);
                }
                catch (Exception commonExeption)
                {
                    _logger.LogError($"Eror in MessageMaster, SendAsync void\n{commonExeption}");

                }
            }
        }
        private async Task SendOneMessage(CancellationToken ct)
        {
            if (_messagesToSend.TryPeek(out var message))
            {
                try
                {
                    _smtpClient = new SmtpClient();
                    await EnsureConnectedAndAuthenticatedAsync(ct);
                    await _smtpClient.SendAsync(message, ct);
                    _smtpClient.Disconnect(true, ct);
                }
                catch (Exception e) when (e is SmtpCommandException or SslHandshakeException or SocketException)
                {
                    throw new ConnectionException(e.Message, innerException: e);
                }
                catch (Exception commonExeption)
                {
                    _logger.LogError($"Eror in MessageMaster, SendAsync void\n{commonExeption}");
                }
            }
        }
        /// <summary>
        /// убиваем таймер, после завершения предыдущего тика, и при получении события _evnt
        /// </summary>
        /// <param name="_timer"></param>
        /// <param name="_evnt"></param>
        /// <returns></returns>
        private bool StopSendTimer(System.Threading.Timer _timer, AutoResetEvent _evnt)
        {            
            if (_timer != null)
            {
                if (_evnt == null)
                {
					_timer.Dispose();
					return true;
                }
				_evnt.WaitOne();
                if (_timer != null)
                {
                    _timer.Dispose();
                }
                return true;
            }
            else return false;
        }
        /// <summary>
        /// Отсановка возможности отправки сообщений. Запуск отправки сообщений пока не реализован.
        /// </summary>
        public void StopSending()
        {
            _killAll = true;
            if (StopSendTimer(_timerSend,_killTimerSignal))
            {
                _logger.LogInformation("timer was killed successfully");
            }
        }
        /// <summary>
        /// отправка сообщений (smtp клеинт ограницен по частоте отправки, сообщения принимаются в очередь и отправляются и определнным интервалом по таймеру)
        /// </summary>
        /// <param name="mailData"></param>
        /// <param name="ct"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public void Send(MailDataModel mailData, CancellationToken ct = default)
        {
            if (mailData.From == null) throw new ArgumentNullException(nameof(mailData.From));
            if (mailData.To == null) throw new ArgumentNullException(nameof(mailData.To));
            if (mailData.Subject == null) throw new ArgumentNullException(nameof(mailData.Subject));
            if (mailData.Body == null) throw new ArgumentNullException(nameof(mailData.Body));

            ThrowIfDisposed();

            MimeMessage mimeMessage = CreateMimeMessage(mailData);
            //ставим в очередь сообщений
            _messagesToSend.Enqueue(mimeMessage);
        }
        /// <summary>
        /// отправка сообщений (smtp клеинт ограницен по частоте отправки, сообщения принимаются в очередь и отправляются и определнным интервалом по таймеру)
        /// </summary>
        /// <param name="mailData"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        public async Task SendAsync(MailDataModel mailData, CancellationToken ct = default)
        {
            if (mailData.From == null) throw new ArgumentNullException(nameof(mailData.From));
            if (mailData.To == null) throw new ArgumentNullException(nameof(mailData.To));
            if (mailData.Subject == null) throw new ArgumentNullException(nameof(mailData.Subject));
            if (mailData.Body == null) throw new ArgumentNullException(nameof(mailData.Body));

            ThrowIfDisposed();

            MimeMessage mimeMessage = CreateMimeMessage(mailData);
            //ставим в очередь сообщение
            _messagesToSend.Enqueue(mimeMessage);
        }
        /// <summary>
        /// конструктор сообщений для MailKit
        /// </summary>
        /// <param name="mailData"></param>
        /// <returns></returns>
        private MimeMessage CreateMimeMessage(MailDataModel mailData)
        {
            var _message = new MimeMessage();
            _message.From.Add(new MailboxAddress(_settings.DisplayName, mailData.From ?? _settings.From));
            _message.Sender = new MailboxAddress(mailData.DisplayName ?? _settings.DisplayName, mailData.From ?? _settings.From);
            _message.Subject = mailData.Subject;
            // Receiver
            foreach (string mailAddress in mailData.To)
                _message.To.Add(MailboxAddress.Parse(mailAddress));
            // Set Reply to if specified in mail data
            if (!string.IsNullOrEmpty(mailData.ReplyTo))
                _message.ReplyTo.Add(new MailboxAddress(mailData.ReplyToName, mailData.ReplyTo));

            // Check if a BCC was supplied in the request
            if (mailData.Bcc != null)
            {
                // Get only addresses where value is not null or with whitespace. x = value of address
                foreach (string mailAddress in mailData.Bcc.Where(x => !string.IsNullOrWhiteSpace(x)))
                    _message.Bcc.Add(MailboxAddress.Parse(mailAddress.Trim()));
            }
            // Check if a CC address was supplied in the request
            if (mailData.Cc != null)
            {
                foreach (string mailAddress in mailData.Cc.Where(x => !string.IsNullOrWhiteSpace(x)))
                    _message.Cc.Add(MailboxAddress.Parse(mailAddress.Trim()));
            }
            _message.Body = new TextPart(TextFormat.Html) { Text = mailData.Body };
            return _message;
        }
        /// <summary>
        /// Проврека, то есть соединение и попытка соединиться
        /// </summary>
        /// <param name="cancellationToken"></param>
        private void EnsureConnectedAndAuthenticated(CancellationToken cancellationToken)
        {
            if (!_smtpClient.IsConnected)
            {
                if (_settings.UseSSL)
                {
                    _smtpClient.Connect(_settings.Host, _settings.Port, true, cancellationToken);
                }
                else
                {
                    _smtpClient.Connect(_settings.Host, _settings.Port, false, cancellationToken);
                }                
            }
            if (!_smtpClient.IsAuthenticated)
            {
                _smtpClient.Authenticate(_settings.UserName, _settings.Password, cancellationToken);
            }
        }
        /// <summary>
        /// Проврека, то есть соединение и попытка соединиться
        /// </summary>
        private void EnsureConnectedAndAuthenticated()
        {
            if (!_smtpClient.IsConnected)
            {
                if (_settings.UseSSL)
                {
                    _smtpClient.Connect(_settings.Host, _settings.Port, true);
                }
                else
                {
                    _smtpClient.Connect(_settings.Host, _settings.Port, false);
                }
            }
            if (!_smtpClient.IsAuthenticated)
            {
                _smtpClient.Authenticate(_settings.UserName, _settings.Password);
            }
        }
        /// <summary>
        /// Проврека, то есть соединение и попытка соединиться
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        private async Task EnsureConnectedAndAuthenticatedAsync(CancellationToken cancellationToken)
        {
            if (!_smtpClient.IsConnected)
            {
                if (_settings.UseSSL)
                {
                    await _smtpClient.ConnectAsync(_settings.Host, _settings.Port, true, cancellationToken);
                }
                else 
                {
                    await _smtpClient.ConnectAsync(_settings.Host, _settings.Port, false, cancellationToken);
                }
            }
            if (!_smtpClient.IsAuthenticated)
            {
                await _smtpClient.AuthenticateAsync(_settings.UserName, _settings.Password, cancellationToken);
            }
        }
    }
}
