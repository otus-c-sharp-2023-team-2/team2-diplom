﻿using AutoMapper;
using MassTransit.NewIdProviders;
using Microsoft.AspNetCore.Mvc;
using Services.Abstractions;
using Services.Contracts;
using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using WebApi.Models;

namespace WebApi.Controllers
{
    /// <summary>
    /// Контроллер каналов уведомлений (смс/электронная почта/телеграм/дискорд)
    /// </summary>
    [ApiController]
    [Route("NotificationService/notificationchannel")]
    public class NotificationChannelController : ControllerBase
    {
        private readonly INotificationChannelService _service;
        private readonly IMapper _mapper;
        public NotificationChannelController(INotificationChannelService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }
        /// <summary>
        /// Получить канал уведомление по ID_канала_уведомления
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            var dto = await _service.GetById(id);
            if (dto == null)
            {
                return NotFound();
            }
            return Ok(_mapper.Map<NotificationChannelModel>(dto));
        }
        /// <summary>
        /// Создать канал уведомлений
        /// </summary>
        /// <remarks>
        /// Пример запроса:
        ///
        ///     POST
        ///     {
        ///        "userId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
        ///        "channelType": 0,
        ///        "adress": "ant4uk@yandex.ru"
        ///     }
        ///
        /// </remarks>
        /// <param name="notificationChannelDto"></param>
        /// <returns></returns>
        [HttpPost]        
        public async Task<IActionResult> Add([FromBody] NotificationChannelModel notificationChannelDto)
        {
            if (notificationChannelDto == null) 
            {
                return BadRequest();
            }
            var res = await _service.Create(_mapper.Map<NotificationChannelModel, NotificationChannelDto>(notificationChannelDto));
            return Ok(res);
        }      
        /// <summary>
        /// Изменить канал уведомлений
        /// </summary>
        /// <param name="notificationChannelDto"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> Edit(NotificationChannelModel notificationChannelDto)
        {            
            await _service.Update(_mapper.Map<NotificationChannelModel, NotificationChannelDto>(notificationChannelDto));
            return Ok();
        }
        /// <summary>
        /// Удалить канал уведомлений по ИД канала
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            await _service.Delete(id);
            return Ok();
        }
        /// <summary>
        /// Удалить все каналы уведомлений по ИД_пользователя
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        [HttpDelete("ByUserID/{userid}")]
        public async Task<IActionResult> DeleteByUserId(Guid userid)
        {
            await _service.DeleteByUserId(userid);            
            return Ok();
        }
        /// <summary>
        /// получить все каналы уведомлений для пользователся с ID
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        [HttpGet("ByUserID/{userid}")]
        public async Task<IActionResult> GetByUserId(Guid userid)
        {
            var dtoList = await _service.GetListByUserId(userid);
            if (dtoList == null || dtoList.Count == 0)
            {
                return NoContent();
            }
            return Ok(dtoList);
        }
    }
}
