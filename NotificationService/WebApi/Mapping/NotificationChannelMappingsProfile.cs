﻿using AutoMapper;
using Services.Contracts;
using WebApi.Models;

namespace WebApi.Mapping
{
    public class NotificationChannelMappingsProfile : Profile
    {
        public NotificationChannelMappingsProfile() 
        {
            CreateMap<NotificationChannelDto, NotificationChannelModel>();//.ReverseMap();
            CreateMap<NotificationChannelModel, NotificationChannelDto>();
        }
    }
}
