﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models
{
    /// <summary>
    /// Канал уведомлений
    /// </summary>
    public class NotificationChannelModel
    {
        /// <summary>
        /// ИД
        /// </summary>
        public Guid Id { get; init; }
        /// <summary>
        /// ИД пользователя
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "User ID required")]
        public Guid UserId { get; init; }
        /// <summary>
        /// Способ уведомлений (почта/телеграм/СМС)
        /// </summary>
        public int ChannelType { get; init; }
        /// <summary>
        /// Адрес канала уведомления почта/телефон
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "Channel required")]
        public string Address { get; init; }
    }
}
