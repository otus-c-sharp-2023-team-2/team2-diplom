﻿using AutoMapper;
using MassTransit;
using MassTransit.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Services.Abstractions;
using Services.Contracts;
using System;
using System.Threading.Tasks;
using WebApi.Models;
using WebApi.MessageMaster;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using WebApi.MessageMaster.Models;
using System.Threading.Channels;
using Microsoft.Extensions.FileSystemGlobbing.Internal;
using System.Text.RegularExpressions;

namespace WebApi.Consumers
{
    /// <summary>
    /// Получатель сообщений
    /// </summary>
    public class NotificationsCommonConsumer : IConsumer<NotificationDto>
    {
        private readonly INotificationService _service;
        private readonly IMapper _mapper;
        private readonly IOptions<MailSettings> _mailSettings;
        private readonly IEmailMaster _emailSender;
        private readonly INotificationChannelService _channelsService;
        private readonly Microsoft.Extensions.Logging.ILogger<NotificationsCommonConsumer> _logger;
        private const string _messageTemplate = "<p>{0}</p><p>Это сообщение сформировано автоматически.</p>" +
                "<p>Если Вы не желаете перестать получать уведомления - отправьте письмо на почту <a data-fr-linked=\"true\" href=\"mailto:otus_team2@mail.ru\">otus_team2@mail.ru</a></p>" +
                "<p>Время отправки {1}</p>";

		public NotificationsCommonConsumer(
            INotificationService service, 
            INotificationChannelService channelsService, 
            IMapper mapper, 
            IOptions<MailSettings> mailSettings, 
            IEmailMaster messageSender,
            Microsoft.Extensions.Logging.ILogger<NotificationsCommonConsumer> logger) 
        {
            _service = service;
            _mapper = mapper;
            _mailSettings = mailSettings;
			_emailSender = messageSender;
            _channelsService = channelsService;
            _logger = logger;
        }
        public async Task Consume(ConsumeContext<NotificationDto> context)
        {            
            if (context == null) 
            {
                //послать сообщение об ошибке в очередь ошибок?
                return;
            }
            if (context.Message.UserID == Guid.Empty)
            {
                //послать сообщение об ошибке в очередь ошибок?
                return;
            }
            //получить канал             
            var channels = await GetNotifcationChannels(context.Message.UserID);
            if (channels.Count == 0) 
            {
                await _channelsService.Create(new NotificationChannelDto() 
                {
                    Address = context.Message.Address,
                    UserId = context.Message.UserID,
                    ChannelType = 0 //по умолчанию всегда добавляется основной канал - почта
                });
                _logger.LogInformation("add new channel for user");
				channels.Add(context.Message.Address);
            }
            foreach ( var channel in channels)
            {
				var res = await SwitchSenders(channel, context.Message);
				if (res == false)
				{
					//послать сообщение об ошибке в очередь ошибок?
				}
			}			
        }
        private async Task<bool> SwitchSenders(string channel, NotificationDto data)
        {			
			if (EmailValidation(channel))
            {
                await EmailSend(data, new List<string>() { channel });
			}
            else
            {
				//phone/TG/WhatsUP master
				_logger.LogInformation("Phone sender is not avaliable");
			}
            return true;
        }
        private bool EmailValidation(string email)
        {
			string _pattern = "[.\\-_a-z0-9]+@([a-z0-9][\\-a-z0-9]+\\.)+[a-z]{2,6}";
			Match isMatch = Regex.Match(email, _pattern, RegexOptions.IgnoreCase);
			return isMatch.Success;
		}
		/// <summary>
		/// отправить сообщение по е-маил
		/// </summary>
		/// <param name="notifDto"></param>
		/// <param name="toAdresses"></param>
		/// <returns></returns>
		private async Task<bool> EmailSend(NotificationDto notifDto, List<string> toAdresses)
        {
            //запись в БД информации о пришедшем уведомлении
            await AddDBNotification(new NotificationModel()
            {
                Body = notifDto.Body,
                Id = Guid.NewGuid(),
                UserID = notifDto.UserID,
                SendingDateTime = notifDto.SendingDateTime
                //Address = notifDto.Address
            });
            //создаем сообщение для отправки
            var _mail = new MailDataModel(toAdresses, 
                "Уведомление о событии в тренажерном зале",
				string.Format(_messageTemplate, notifDto.Body, notifDto.SendingDateTime),
                _mailSettings.Value.From,
                null, null, null, null, null);    
            //отправляем сообщение
            await _emailSender.SendAsync(_mail);
            return true;
        }
        /// <summary>
        /// запись в БД о уведомлении
        /// </summary>
        /// <param name="notificationDto"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        private async Task AddDBNotification(NotificationModel notificationDto)
        {
            if (notificationDto == null)
            {
                throw new ArgumentNullException(nameof(notificationDto));
            }
            var _not = new NotificationDto() 
            {
                SendingDateTime=notificationDto.SendingDateTime,
                Body=notificationDto.Body,
                UserID=notificationDto.UserID,
                Address = notificationDto.Address
            };
            await _service.Create(_not);
        }
        /// <summary>
        /// получить все каналы для рассылки
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        private async Task<List<string>> GetNotifcationChannels(Guid id)
        {
            if (id == Guid.Empty)
            {
                throw new ArgumentNullException(nameof(Guid));
            }
            var res = await _channelsService.GetListByUserId(id);
			List<string> chList = new();
			foreach (var channel in res)
			{
				chList.Add(channel.Address);
			}
			return chList;
        }
    }
	/// <summary>
	/// не создает дополнителные Exchange/queue в RabbitMQ
	/// </summary>
	public class NotificationsCommonDefinition : ConsumerDefinition<NotificationsCommonConsumer>
    {
        protected override void ConfigureConsumer(IReceiveEndpointConfigurator endpointConfigurator, IConsumerConfigurator<NotificationsCommonConsumer> consumerConfigurator)
        {            
            endpointConfigurator.ConfigureConsumeTopology = false; //не создается Exchange для NotificationDto
        }
    }    
}
