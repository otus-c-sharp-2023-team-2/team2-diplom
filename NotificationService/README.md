#NotificationService
## Формат уведомлений
Уведомление имеет следующую структуру:
```csharp
public class NotificationDto
    {
        public string Body { get; set; }
        public DateTime SendingDateTime { get; set; }
        public Guid UserID { get; set; }
    }
```
В поле Body лучше писать html текст.
В настойший момент шаблоны сообщений находятся в разработке.
Все сообщения будут выглядеть следующим образом:
```html
<p>Здравствуйте!</p>
<p>Вас приветствует тренажерный зал &quot;Щегы-еуфь2&quot;.</p>
<p>Вы получили уведомление ВАШЕ СООБЩЕНИЕ</p>
<p>Это сообщение сформировано автоматически.</p>
<p>Если Вы не желаете перестать получать уведомления - отправьте письмо на почту <a data-fr-linked="true" href="mailto:otus_team2@mail.ru">otus_team2@mail.ru</a></p>
<p>Время отправки ВРЕМЯ ОТПРАВКИ</p>
```
Ваше сообщение из поля Body будет вставлено вместо слов ВАШЕ СООБЩЕНИЕ. Время и дата отправки будут вставлены вместо слов ВРЕМЯ ОТПРАВКИ.
В письме это будет выглядеть следующим образом:

<p>Здравствуйте!</p>
<p>Вас приветствует тренажерный зал &quot;Щегы-еуфь2&quot;.</p>
<p>Вы получили уведомление ВАШЕ СООБЩЕНИЕ</p>
<p>Это сообщение сформировано автоматически.</p>
<p>Если Вы не желаете перестать получать уведомления - отправьте письмо на почту <a data-fr-linked="true" href="mailto:otus_team2@mail.ru">otus_team2@mail.ru</a></p>
<p>Время отправки ВРЕМЯ ОТПРАВКИ</p>

## REST API для сервиса Notifications
<details>
  <summary>GET /notifications/{id}</summary>
Получить уведомление по ID (не ясно зачем, возможно нужно сделать по ID_User)
</details>
<details>
  <summary>POST /notifications</summary>
НЕ использовать, используется на этапах разработки
Отправить уведомление через RabbitMq
</details>

------------


<details>
  <summary>GET NotificationService/notificationchannel/{id}</summary>
Получить канал уведомления по ИД
</details>
<details>
  <summary>PUT NotificationService/notificationchannel/{id}</summary>
Изменить канал уведомления
</details>
<details>
  <summary>DELETE NotificationService/notificationchannel/{id}</summary>
удалить канал уведомлений
</details>
<details>
  <summary>POST NotificationService/notificationchannel</summary>
Создать канал уведомлений
Value
{
  "userId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "channelType": 1,
  "adress": "mymail@mail.ug"
}
</details>
<details>
  <summary>DELETE NotificationService/notificationchannel/ByUserID/{userid}</summary>
Удаляет все каналы для пользователя с ИД
</details>
<details>
  <summary>GET NotificationService/notificationchannel/ByUserID/{userid}</summary>
Получить все каналы для пользователя с ИД
</details>

## HOW-TO
1. Нужен Guid пользователя (создаем или запрашиваем)
2. Создаем для этого пользоваиеля каналы уведомлений POST NotificationService/notificationchannel
3. Теперь можно отправлять уведомления в RabbitMq на exchange "exchange:NotificationsCommon"

## Пример использования RabbitMq в сервисе
(кустарный мой способ)
1. В appsettings добавляем запись
```
"RabbitMq": {
    "Username": "rmuser",
    "Password": "rmpassword",
    "Host": "localhost",
    "VirtualHost":  "/"
  }
```
2. В Контрактах добавляем Dto (возможно надо перенести в Модели? в WebAPI)
```csharp
public class NotificationDto
    {
        public Guid Id { get; set; } //можно не добавлять. должно без него работать
        public string Body { get; set; }
        public DateTime SendingDateTime { get; set; }
        public Guid UserID { get; set; }
    }
```

3. В Startup конфигурируем MassTransit

```csharp
using WebApi.Settings;

services.AddMassTransit(x => 
            {
                x.AddConsumer<NotificationsCommon, NotificationsCommonDefinition>(); //add consumer
                x.UsingRabbitMq((context, cfg) =>
                {
                    Configure(cfg, Configuration);
                    x.SetEndpointNameFormatter(new KebabCaseEndpointNameFormatter(false)); // уточняем формат названия очереди и точки обмена                  
                    cfg.ConfigureEndpoints(context);  //consumer registration 
                });                
            }
            );
 private static void Configure(IRabbitMqBusFactoryConfigurator configurator , Microsoft.Extensions.Configuration.IConfiguration conf)
        {
            configurator.Host(conf["RabbitMq:Host"],  
                conf["RabbitMq:VirtualHost"],
                h =>
                {
                    h.Username(conf["RabbitMq:Username"]);
                    h.Password(conf["RabbitMq:Password"]);
                });
        }

```

4. В контроллере:
Добавляем поле получения конечной точки
```csharp
private readonly ISendEndpointProvider _sendEndpointProvider;
```
Конструктор
```csharp
public NotificationController(INotificationService service, IMapper mapper, ISendEndpointProvider sendEndpointProvider)
        {
            _service = service;
            _mapper = mapper;
            _sendEndpointProvider = sendEndpointProvider;
        }
```
В методе Add/Create или любом другом, из которого нужно послать сообщение:
```csharp
await SendTest(notificationDto);
```

Добавляем метод(задачу) по посылке сообщения
```csharp
private async Task SendTest (NotificationModel notificationDto) //CancellationToken stoppingtoken
        {
            var endpoint = await _sendEndpointProvider.GetSendEndpoint(new Uri("exchange:NotificationsCommon"));
            await endpoint.Send(new NotificationDto 
            { 
                Id= notificationDto.Id, 
                Body = notificationDto.Body,
                UserID = notificationDto.UserID,
                SendingDateTime = notificationDto.SendingDateTime
            });
        }
```

TODO 
- название очередей/точек  обмена запихнуть в appsettings.json
- CancellationToken для задачи посылки уведомления