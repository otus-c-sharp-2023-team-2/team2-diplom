﻿using AutoMapper;
using Domain.Entities;
using Microsoft.Extensions.Logging;
using Services.Abstractions;
using Services.Contracts;
using Services.Implementations.Exceptions;
using Services.Repositories.Abstractions;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.Implementations
{
    /// <summary>
    /// Сервис работы с пользователями.
    /// </summary>
    public class UserService : IUserService
    {
        private readonly IMapper _mapper;
        private readonly IUserRepository _repository;
        private readonly IRabbitMqService _rabbitMqService;
		private readonly ILogger _log;

		public UserService(
            IMapper mapper,
            IUserRepository repository,
            IRabbitMqService rabbitMqService,
			ILogger<UserService> log)
        {
            _mapper = mapper;
            _repository = repository;
            _rabbitMqService = rabbitMqService;
            _log = log;
        }

        /// <summary>
        /// Получить постраничный список.
        /// </summary>
        /// <param name="filterDto"> ДТО фильтра. </param>
        /// <returns> Список элементов. </returns>
        public async Task<ICollection<UserDto>> GetPaged(UserFilterDto filterDto)
        {
            ICollection<User> entities = await _repository.GetPagedAsync(filterDto);
            return _mapper.Map<ICollection<User>, ICollection<UserDto>>(entities);
        }

        /// <summary>
        /// Получить по
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <returns>ДТО элемента</returns>
        public async Task<UserDto> GetById(Guid id)
        {
            var item = await _repository.GetAsync(id);

            if (item is null)
                throw new ItemNotFoundException(id, nameof(User));

            return _mapper.Map<User, UserDto>(item);
        }

        /// <summary>
        /// Создать
        /// </summary>
        /// <param name="userDto">ДТО элемента</param>
        /// <returns>идентификатор</returns>
        public async Task<Guid> Create(UserDto userDto)
        {
            var entity = _mapper.Map<UserDto, User>(userDto);


            if (!userDto.Id.Equals(Guid.Empty))
            {
                var existed = await _repository.GetAsync(userDto.Id);
                if (existed != null)
                    throw new ItemAlreadyExistException(userDto.Id, nameof(User));
            }

            var res = await _repository.AddAsync(entity);
            await _repository.SaveChangesAsync();

            userDto.Id = res.Id;
            try
            {
                await _rabbitMqService.SendWelcomeMessage(userDto);
            }
            catch(Exception ex)
            {
                _log.LogWarning(ex.Message);
            }            

            return res.Id;
        }

        /// <summary>
        /// Изменить
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <param name="userDto">ДТО элемента</param>
        public async Task Update(Guid id, UserDto userDto)
        {
            var entity = _mapper.Map<UserDto, User>(userDto);
            entity.Id = id;
            _repository.Update(entity);
            await _repository.SaveChangesAsync();
        }

        /// <summary>
        /// Удалить
        /// </summary>
        /// <param name="id">идентификатор</param>
        public async Task Delete(Guid id)
        {
            var item = _repository.Delete(id);
            await _repository.SaveChangesAsync();
        }
    }
}