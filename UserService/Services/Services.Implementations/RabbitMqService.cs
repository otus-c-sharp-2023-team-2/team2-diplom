﻿using Domain.Entities;
using MassTransit;
using RabbitMQ.Client;
using Services.Abstractions;
using Services.Contracts;
using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Services.Implementations;

/// <inheritdoc cref="IRabbitMqService"/>
public class RabbitMqService : IRabbitMqService
{
    private readonly ISendEndpointProvider _sendEndpointProvider;
    private const string WELCOME_MSG_BODY = "Здравствуйте, {0} {1}! Вы успешно зарегистрировались на сайте.";

    public RabbitMqService(ISendEndpointProvider sendEndpointProvider)
    {
        _sendEndpointProvider = sendEndpointProvider;
    }

    public async Task SendWelcomeMessage(UserDto user)
    {
        await ProduceAsync(new NotificationDto() {
            Id = Guid.NewGuid(),
            Body = string.Format(WELCOME_MSG_BODY, user.FirstName, user.LastName),
            UserID = user.Id,
            SendingDateTime = DateTime.UtcNow,
            Address = user.Email
        });
    }

    private async Task ProduceAsync(NotificationDto notificationDto, CancellationToken cancellationToken = default(CancellationToken))
    {
        var endpoint = await _sendEndpointProvider.GetSendEndpoint(new Uri("rabbitmq://host.docker.internal/NotificationsCommon"));
        await endpoint.Send(notificationDto, cancellationToken);
    }
}
