﻿using Microsoft.AspNetCore.Http;
using System;

namespace Services.Implementations;

public class ItemNotFoundException : HttpException
{
    private const string MESSAGE = "Элемент {0} с Id {1} не найден в базе";

    public ItemNotFoundException(Guid id, string itemName)
        : base(
              httpStatusCode: StatusCodes.Status404NotFound,
              message: string.Format(MESSAGE, itemName, id))
    {
    }
}
