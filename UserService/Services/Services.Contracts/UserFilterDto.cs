namespace Services.Contracts;

public class UserFilterDto : PagedFilterDto
{
    public string FullName { get; set; }
}