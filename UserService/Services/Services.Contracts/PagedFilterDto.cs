namespace Services.Contracts;

public class PagedFilterDto
{       
    public int ItemsPerPage { get; set; }
    
    public int Page { get; set; }
}