﻿using System;

namespace Services.Contracts;

public class NotificationDto
{
    public Guid Id { get; set; }
    public string Body { get; set; }
    public DateTime SendingDateTime { get; set; }
    public Guid UserID { get; set; }
    public string Address { get; set; }
}