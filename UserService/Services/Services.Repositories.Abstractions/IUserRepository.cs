﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Entities;
using Services.Contracts;

namespace Services.Repositories.Abstractions
{
    /// <summary>
    /// Репозиторий работы с пользователями.
    /// </summary>
    public interface IUserRepository : IRepository<User, Guid>
    {
        Task<List<User>> GetPagedAsync(UserFilterDto filterDto);
    }
}
