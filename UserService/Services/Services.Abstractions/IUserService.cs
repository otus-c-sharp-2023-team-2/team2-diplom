using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Services.Contracts;

namespace Services.Abstractions
{
    /// <summary>
    /// Сервис работы с пользователями
    /// </summary>
    public interface IUserService : IService<UserDto, Guid>
    {
        /// <summary>
        /// Получить постраничный список.
        /// </summary>
        /// <param name="filterDto"> ДТО фильтра. </param>
        /// <returns> Список элементов. </returns>
        Task<ICollection<UserDto>> GetPaged(UserFilterDto filterDto);
    }
}