using System.Collections.Generic;
using System.Threading.Tasks;
using Services.Contracts;

namespace Services.Abstractions
{
    /// <summary>
    /// Обобщенный CRUD сервис работы с данными.
    /// </summary>
    public interface IService<T, TId>
    {
        /// <summary>
        /// Получить
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <returns>ДТО элемент</returns>
        Task<T> GetById(TId id);

        /// <summary>
        /// Создать
        /// </summary>
        /// <param name="courseDto">ДТО элемент</para
        Task<TId> Create(T courseDto);

        /// <summary>
        /// Изменить
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <param name="courseDto">ДТО элемент</param>
        Task Update(TId id, T courseDto);

        /// <summary>
        /// Удалить
        /// </summary>
        /// <param name="id">идентификатор</param>
        Task Delete(TId id);
    }
}