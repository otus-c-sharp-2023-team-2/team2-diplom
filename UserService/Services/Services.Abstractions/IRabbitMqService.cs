﻿using Services.Contracts;
using System;
using System.Threading.Tasks;

namespace Services.Abstractions;

/// <summary>
/// Сервис работы с RabbitMq.
/// </summary>
public interface IRabbitMqService
{
    /// <summary>
    /// Отправка приветственного уведомления пользователю.
    /// </summary>
    Task SendWelcomeMessage(UserDto user);
}
