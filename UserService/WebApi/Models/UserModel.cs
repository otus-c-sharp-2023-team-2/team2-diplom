﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models;

public class UserModel
{
    public Guid Id { get; set; }

    [MaxLength(100)]
    public string FirstName { get; set; }

    [MaxLength(100)]
    public string LastName { get; set; }

    [RegularExpression("^\\S+@\\S+\\.\\S+$", ErrorMessage = "Неверный формат Email.")]
    public string Email { get; set; }

    public long PhoneNumber { get; set; }

    public string Promocode { get; set; }
}
