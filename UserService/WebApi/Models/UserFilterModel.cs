namespace WebApi.Models;

public class UserFilterModel
{
    public string FullName { get; set; }
    
    public int ItemsPerPage { get; set; }
    
    public int Page { get; set; }
}