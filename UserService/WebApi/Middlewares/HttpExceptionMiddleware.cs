﻿using Microsoft.AspNetCore.Http;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using System;
using System.Text.Json;
using Services.Implementations;

namespace WebApi.Middlewares;

/// <summary>
/// Middleware для обработки http исключений.
/// </summary>
public class HttpExceptionMiddleware
{
    private readonly RequestDelegate next;

    public HttpExceptionMiddleware(RequestDelegate next)
    {
        this.next = next;
    }

    public async Task Invoke(HttpContext context)
    {
        try
        {
            await this.next.Invoke(context);
        }
        catch (HttpException httpException)
        {
            context.Response.Clear();
            context.Response.StatusCode = httpException.StatusCode;
            context.Response.ContentType = MediaTypeNames.Application.Json;
            await context.Response.Body.WriteAsync(Encoding.UTF8.GetBytes(
                   JsonSerializer.Serialize(new
                   {
                       Message = httpException.Message,
                   })));
        }
        catch (Exception ex)
        {
            context.Response.Clear();
            context.Response.StatusCode = StatusCodes.Status500InternalServerError;
            context.Response.ContentType = MediaTypeNames.Application.Json;
            await context.Response.Body.WriteAsync(Encoding.UTF8.GetBytes(
                   JsonSerializer.Serialize(new
                   {
                       Message = ex.Message,
                   })));
        }
    }
}
