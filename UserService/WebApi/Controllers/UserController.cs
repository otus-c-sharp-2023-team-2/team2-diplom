﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Services.Abstractions;
using Services.Contracts;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebApi.Models;

namespace WebApi.Controllers;

/// <summary>
/// Контроллер работы с пользователями.
/// </summary>
[ApiController]
[Route("[controller]")]
public class UserController : ControllerBase
{
    private readonly IUserService _service;
    private readonly ILogger<UserController> _logger;
    private IMapper _mapper;
    public UserController(IUserService service, ILogger<UserController> logger, IMapper mapper)
    {
        _service = service;
        _logger = logger;
        _mapper = mapper;
    }

    /// <summary>
    /// Получить элемент по id.
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpGet("{id}")]
    public async Task<IActionResult> GetAsync(Guid id)
    {
        var itemDto = await _service.GetById(id);
        return Ok(_mapper.Map<UserDto, UserModel>(itemDto));
    }

    /// <summary>
    /// Получить элементы списком.
    /// </summary>
    /// <param name="filterModel">Модель фильтров / постраничного разделения.</param>
    /// <returns></returns>
    [HttpGet("list")]
    public async Task<IActionResult> GetListAsync([FromQuery] UserFilterModel filterModel)
    {
        var filterDto = _mapper.Map<UserFilterModel, UserFilterDto>(filterModel);
        return Ok(_mapper.Map<List<UserModel>>(await _service.GetPaged(filterDto)));
    }

    /// <summary>
    /// Добавить элемент.
    /// </summary>
    /// <param name="itemDto"></param>
    /// <returns></returns>
    [HttpPost]
    public async Task<IActionResult> AddAsync(UserModel itemDto)
    {
        return Ok(await _service.Create(_mapper.Map<UserDto>(itemDto)));
    }

    /// <summary>
    /// Обновить элемент.
    /// </summary>
    /// <param name="id"></param>
    /// <param name="itemDto"></param>
    /// <returns></returns>
    [HttpPut("{id}")]
    public async Task<IActionResult> UpdateAsync(Guid id, UserModel itemDto)
    {
        await _service.Update(id, _mapper.Map<UserDto>(itemDto));
        return Ok();
    }

    /// <summary>
    /// Удалить элемент.
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteAsync(Guid id)
    {
        await _service.Delete(id);
        return Ok();
    }

    
}
