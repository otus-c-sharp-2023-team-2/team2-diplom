﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;

namespace Infrastructure.EntityFramework.Extensions;
public static class ModelBuilderExtensions
{
    public static void Seed(this ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<User>()
            .HasData(
                new User
                {
                    Id = Guid.Parse("cad4244b-ffaf-48a0-b798-ea41b4cce215"),
                    Email = "tatyana.antipova@gmail.com",
                    FirstName = "Татьяна",
                    LastName = "Антипова",
                    PhoneNumber = 79030001234
                });

        modelBuilder.Entity<User>()
            .HasData(
                new User
                {
                    Id = Guid.Parse("103d165c-6062-4470-a318-09ca1a5350cc"),
                    Email = "testUser1@gmail.com",
                    FirstName = "Тестовый",
                    LastName = "Пользователь",
                    PhoneNumber = 79030005432
                });

        modelBuilder.Entity<User>()
            .HasData(
                new User
                {
                    Id = Guid.Parse("82f707bb-044e-48e7-af30-67db8bbbb00b"),
                    Email = "testUser2@gmail.com",
                    FirstName = "Другой",
                    LastName = "Пользователь",
                    PhoneNumber = 79030001986
                });
    }
}
