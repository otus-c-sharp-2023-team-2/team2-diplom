﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Services.Repositories.Abstractions;
using Domain.Entities;
using Infrastructure.EntityFramework;
using Microsoft.EntityFrameworkCore;
using System;
using Services.Contracts;

namespace Infrastructure.Repositories.Implementations
{
    /// <summary>
    /// Репозиторий работы с пользователями.
    /// </summary>
    public class UserRepository : Repository<User, Guid>, IUserRepository
    {
        public UserRepository(DatabaseContext context) : base(context)
        {
        }

        /// <summary>
        /// Получить постраничный список.
        /// </summary>
        /// <param name="filterDto"> ДТО фильтра. </param>
        /// <returns> Список элементов. </returns>
        public async Task<List<User>> GetPagedAsync(UserFilterDto filterDto)
        {

            var query = GetAll().ToList().AsQueryable();

            if (!string.IsNullOrWhiteSpace(filterDto.FullName))
            {
                query = query.Where(c => c.FirstName.Contains(filterDto.FullName) 
                || c.LastName.Contains(filterDto.FullName));
            }

            if(filterDto.ItemsPerPage != 0 || filterDto.Page != 0)
                query = query
                    .Skip((filterDto.Page - 1) * filterDto.ItemsPerPage)
                    .Take(filterDto.ItemsPerPage);

            return query.ToList();
        }

        /// <summary>
        /// Получить сущность по ID
        /// </summary>
        /// <param name="id">ID сущности</param>
        /// <returns>сущность</returns>
        public override Task<User> GetAsync(Guid id)
        {
            var query = Context.Set<User>().AsQueryable();
            query = query
                //.Include(c => c.Lessons)
                .Where(c => c.Id == id);

            return query.SingleOrDefaultAsync();
        }
    }
}
