﻿using System;
using System.Collections.Generic;

namespace Domain.Entities;

/// <summary>
/// Пользователь.
/// </summary>
public  class User : IEntity<Guid>
{
    public Guid Id { get; set; }

    public string FirstName { get; set; }

    public string LastName { get; set; }

    public string Email { get; set; }

    public long PhoneNumber { get; set; }

    public string Promocode { get; set; }
}
