using AppointmentManagerApp.Extensions;
using AppointmentManagerApp.Mapping;
using AppointmentManagerApp.Middlewares;
using AppointmentManagerApp.Models.Common;
using AppointmentManagerApp.Providers.Auth;
using AppointmentManagerApp.Providers.Benefit;
using AppointmentManagerApp.Providers.Notification;
using AppointmentManagerApp.Providers.Schedule;
using AppointmentManagerApp.Providers.User;
using AppointmentManagerApp.Services.Auth;
using AppointmentManagerApp.Services.Membership;
using AppointmentManagerApp.Services.Notification;
using AppointmentManagerApp.Services.Schedule;
using AppointmentManagerApp.Services.User;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Cryptography;

var builder = WebApplication.CreateBuilder(args);

var jwtSettingsConfiguration = builder.Configuration.GetSection("AccessTokenSettings");
builder.Services.Configure<AccessTokenSettings>(jwtSettingsConfiguration);
var jwtSettings = jwtSettingsConfiguration.Get<AccessTokenSettings>();

var commonSettingsConfiguration = builder.Configuration.GetSection("AppointmentManagerApp");
builder.Services.Configure<AppointmentManagerAppSettings>(commonSettingsConfiguration);
var commonSettings = commonSettingsConfiguration.Get<AppointmentManagerAppSettings>();

RSA rsa = RSA.Create();
rsa.ImportRSAPublicKey(
    source: Convert.FromBase64String(jwtSettings.PublicKey),
    bytesRead: out int _
);

var rsaKey = new RsaSecurityKey(rsa);
JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();
builder.Services.AddAuthentication(options =>
{
    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
})
.AddJwtBearer(options =>
{
    options.RequireHttpsMetadata = false;
    options.TokenValidationParameters = new TokenValidationParameters
    {
        ValidateIssuer = true,
        ValidateAudience = true,
        ValidateLifetime = true,
        RequireSignedTokens = true,
        RequireExpirationTime = true,
        ValidateIssuerSigningKey = true,
        ValidIssuer = jwtSettings.Issuer,
        ValidAudience = jwtSettings.Audience,
        IssuerSigningKey = rsaKey,
        ClockSkew = jwtSettings.JwtTokenClockSkew,
        NameClaimType = "name",
        RoleClaimType = "role",
    };
});

builder.Services.AddAuthorization();

// Add services to the container.
builder.Services.AddControllersWithViews();

builder.Services.AddDistributedMemoryCache();

builder.Services.AddSession(options =>
{
    options.IdleTimeout = TimeSpan.FromMinutes(10);
    options.Cookie.HttpOnly = true;
    options.Cookie.IsEssential = true;
});

builder.Services.AddHttpClient(Constants.AUTH_HTTP_CLIENT_NAME, c =>
{
    c.DefaultRequestHeaders.Add("Accept", "application/json; charset=UTF-8");
    c.BaseAddress = new Uri(jwtSettings.Issuer);
    c.Timeout = TimeSpan.FromSeconds(30);
})
.AddHttpMessageHandler<BearerTokenHandler>();

builder.Services.AddHttpClient(Constants.BENEFIT_HTTP_CLIENT_NAME, c =>
{
    c.DefaultRequestHeaders.Add("Accept", "application/json; charset=UTF-8");
    c.BaseAddress = new Uri(commonSettings.BenefitServiceUrl);
    c.Timeout = TimeSpan.FromSeconds(30);
});

builder.Services.AddHttpClient(Constants.Notification_HTTP_CLIENT_NAME, c =>
{
    c.DefaultRequestHeaders.Add("Accept", "application/json; charset=UTF-8");
    c.BaseAddress = new Uri(commonSettings.NotificationUrl);
    c.Timeout = TimeSpan.FromSeconds(30);
});

builder.Services.AddHttpClient(Constants.USER_HTTP_CLIENT_NAME, c =>
{
    c.DefaultRequestHeaders.Add("Accept", "application/json; charset=UTF-8");
    c.BaseAddress = new Uri(commonSettings.UserServiceUrl);
    c.Timeout = TimeSpan.FromSeconds(30);
});

builder.Services.AddHttpContextAccessor();
builder.Services.AddTransient<JwtTokenMiddleware>();
builder.Services.AddScoped<BearerTokenHandler>();
builder.Services.AddScoped<IAuthService, AuthService>();
builder.Services.AddScoped<IAuthProvider, AuthProvider>();
builder.Services.AddScoped<IUserService, UserService>();
builder.Services.AddScoped<IUserProvider, UserProvider>();
builder.Services.AddScoped<INotificationService, NotificationService>();
builder.Services.AddScoped<INotificationProvider, NotificationProvider>();
builder.Services.AddScoped<IScheduleService, ScheduleService>();
builder.Services.AddScoped<IScheduleProvider, ScheduleProvider>();
builder.Services.AddScoped<IBenefitProvider, BenefitProvider>();
builder.Services.AddScoped<IMembershipService, MembershipService>();

var mapperConfig = new MapperConfiguration(mc =>
{
    mc.AddProfile(new UserMappingsProfile());
    mc.AddProfile(new MembershipMappingsProfile());
});

IMapper mapper = mapperConfig.CreateMapper();
builder.Services.AddSingleton(mapper);
var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseCookiePolicy();

app.UseCors(builder =>
    builder.AllowAnyOrigin()
    .AllowAnyHeader()
    .AllowAnyMethod()
);

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseFactoryActivatedMiddleware();

app.UseAuthentication();
app.UseAuthorization();

app.UseSession();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();


//2209 work