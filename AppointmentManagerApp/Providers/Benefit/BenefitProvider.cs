﻿using AppointmentManagerApp.Models.Benefit.Requests;
using AppointmentManagerApp.Models.Common;
using AppointmentManagerApp.Models.User.Domain;
using AppointmentManagerApp.Models.User.Requests;
using System.Threading;

namespace AppointmentManagerApp.Providers.Benefit;

/// <inheritdoc cref="IBenefitProvider"/>
public class BenefitProvider : ProviderBase, IBenefitProvider
{
    private readonly IHttpClientFactory _clientFactory;

    public BenefitProvider(IHttpClientFactory clientFactory)
    {
        _clientFactory = clientFactory;
    }

   
    public async Task ApplyRegitrationAsync(ApplyRegitrationRequest request)
    {
		var client = _clientFactory.CreateClient(Constants.BENEFIT_HTTP_CLIENT_NAME);

		try
		{
			using var message = await client.PostAsJsonAsync<ApplyRegitrationRequest>("benefit/reg", request);

			if (!message.IsSuccessStatusCode)
			{
				throw new BadHttpRequestException("Не удалось выполнить запрос.", (int)message.StatusCode);
            }
			return;
		}
		catch (Exception ex)
		{
		}
	}

    public async Task<GetUserBenefitResponse> GetDiscountBenefitAsync(GetUserBenefitRequest request)
    {
        var client = _clientFactory.CreateClient(Constants.BENEFIT_HTTP_CLIENT_NAME);

        try
        {
            using var message = await client.PostAsJsonAsync<GetUserBenefitRequest>("benefit/getBenefit", request);

            if (!message.IsSuccessStatusCode)
            {
                throw new BadHttpRequestException("Не удалось выполнить запрос.", (int)message.StatusCode);
            }

            var result = await message.Content?.ReadFromJsonAsync<GetUserBenefitResponse>();

            return result;
        }
        catch (Exception ex)
        {
        }
        return default(GetUserBenefitResponse);
    }
}
