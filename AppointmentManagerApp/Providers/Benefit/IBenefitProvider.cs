﻿using AppointmentManagerApp.Models.Benefit.Requests;
using AppointmentManagerApp.Models.User.Domain;
using AppointmentManagerApp.Models.User.Requests;

namespace AppointmentManagerApp.Providers.Benefit;

/// <summary>
/// Провайдер пользователей.
/// </summary>
public interface IBenefitProvider
{ 
    Task ApplyRegitrationAsync(ApplyRegitrationRequest request);

    Task<GetUserBenefitResponse> GetDiscountBenefitAsync(GetUserBenefitRequest request);
}
