﻿using AppointmentManagerApp.Models.Notification.Requests;

namespace AppointmentManagerApp.Providers.Notification
{
    /// <summary>
    /// Провайдер уведомлений (обслуживает и уведомления и каналы уведомлений)
    /// </summary>
    public interface INotificationProvider
    {
        /// <summary>
        /// Создать новый канал уведомлений
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<CreateNotificationChannelResponse> CreateNotificationChannelAsync(CreateNotificationChannelRequest request, CancellationToken cancellationToken = default);
        Task<NotificationChannelResponse> GetNotificationChannelsAsync(Guid Id, CancellationToken cancellationToken = default);   
        Task<bool> CreateNotification(Guid Id, string message, CancellationToken cancellationToken = default);
    }
}
