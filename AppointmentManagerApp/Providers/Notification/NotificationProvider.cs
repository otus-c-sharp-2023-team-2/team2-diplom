﻿using AppointmentManagerApp.Models.Notification.Requests;
using AppointmentManagerApp.Models.Common;

namespace AppointmentManagerApp.Providers.Notification
{
    /// <inheritdoc cref="INotificationProvider"/>
    public class NotificationProvider : ProviderBase, INotificationProvider
    {
        private readonly IHttpClientFactory _clientFactory;
        public NotificationProvider(IHttpClientFactory clientFactory) 
        {
            _clientFactory = clientFactory;
        }
        public async Task<CreateNotificationChannelResponse> CreateNotificationChannelAsync(CreateNotificationChannelRequest request, CancellationToken cancellationToken)
        {
            var res = await PostAsync<CreateNotificationChannelRequest, CreateNotificationChannelResponse>(
                _clientFactory,
                Constants.Notification_HTTP_CLIENT_NAME,
                request,
                "NotificationService/notificationchannel",
                cancellationToken);
            return res;
        }
        public async Task<NotificationChannelResponse> GetNotificationChannelsAsync(Guid id, CancellationToken token)
        {
            var chList = await GetResultOrDefaultAsync<List<NotificationChannel>>(
                _clientFactory,
                Constants.Notification_HTTP_CLIENT_NAME,
				$"NotificationService/notificationchannel/ByUserID/{id}",
                token);

            var res = new NotificationChannelResponse { Channels = chList ?? Enumerable.Empty<NotificationChannel>() };
            
            return res;
        }
        public async Task<bool> CreateNotification(Guid id,string message, CancellationToken token)
        {
            var _request = new CreateNotificationRequest {
                UserID = id, 
                Body = message,
                SendingDateTime = DateTime.UtcNow,
            };
            var res = await PostAsync<CreateNotificationRequest, CreateNotificationResponse>(
                _clientFactory,
                Constants.Notification_HTTP_CLIENT_NAME,
				_request,
				$"NotificationService/notification",
                token);

            return true;
        }



    }
}
