﻿using AppointmentManagerApp.Models.Auth.Requests;
using AppointmentManagerApp.Models.Common;

namespace AppointmentManagerApp.Providers.Auth;

/// <inheritdoc cref="IAuthProvider"/>
public class AuthProvider : ProviderBase, IAuthProvider
{
    private readonly IHttpClientFactory _clientFactory;

    public AuthProvider(IHttpClientFactory clientFactory)
    {
        _clientFactory = clientFactory;
    }

    public async Task<LoginResponse> LoginAsync(LoginRequest request, CancellationToken cancellationToken)
    {
        return await PostAsync<LoginRequest, LoginResponse>(
            _clientFactory,
            Constants.AUTH_HTTP_CLIENT_NAME,
            request, 
            "auth/login", 
            cancellationToken);
    }

    public async Task<RefreshTokenResponse> RefreshTokenAsync(RefreshTokenRequest request, CancellationToken cancellationToken)
    {
        return await PostAsync<RefreshTokenRequest, RefreshTokenResponse>(
            _clientFactory,
            Constants.AUTH_HTTP_CLIENT_NAME, 
            request, 
            "auth/RefreshToken",
            cancellationToken);
    }

    public async Task<SignOutResponse> SignOutAsync(SignOutRequest request, CancellationToken cancellationToken)
    {
        return await PostAsync<SignOutRequest, SignOutResponse>(
            _clientFactory,
            Constants.AUTH_HTTP_CLIENT_NAME, 
            request, 
            "auth/SignOut", 
            cancellationToken);
    }

    public async Task<AuthCreateUserResponse> CreateUserAsync(AuthCreateUserRequest request, CancellationToken cancellationToken)
    {
        return await PostAsync<AuthCreateUserRequest, AuthCreateUserResponse>(
            _clientFactory,
            Constants.AUTH_HTTP_CLIENT_NAME, 
            request, 
            "user", 
            cancellationToken);
    }

    public async Task<AuthUpdateUserResponse> UpdateUserAsync(AuthUpdateUserRequest request, CancellationToken cancellationToken)
    {
        return await PutAsync<AuthUpdateUserRequest, AuthUpdateUserResponse>(
            _clientFactory,
            Constants.AUTH_HTTP_CLIENT_NAME,
            request,
            "user",
            cancellationToken);
    }

    public async Task<AuthGetUsersRolesListResponse> GetUsersRolesListAsync(CancellationToken cancellationToken)
    {
        return await GetAsync<AuthGetUsersRolesListResponse>(
            _clientFactory,
            Constants.AUTH_HTTP_CLIENT_NAME,
            "user",
            cancellationToken);
    }
}
