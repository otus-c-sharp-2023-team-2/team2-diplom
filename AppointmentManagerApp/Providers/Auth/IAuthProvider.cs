﻿using AppointmentManagerApp.Models.Auth.Requests;

namespace AppointmentManagerApp.Providers.Auth;

/// <summary>
/// Провайдер авторизации.
/// </summary>
public interface IAuthProvider
{
    /// <summary>
    /// Залогинить пользователя.
    /// </summary>
    Task<LoginResponse> LoginAsync(LoginRequest request, CancellationToken cancellationToken);

    /// <summary>
    /// Обновить токены доступа.
    /// </summary>
    Task<RefreshTokenResponse> RefreshTokenAsync(RefreshTokenRequest request, CancellationToken cancellationToken);

    /// <summary>
    /// Разлогинить пользователя.
    /// </summary>
    Task<SignOutResponse> SignOutAsync(SignOutRequest request, CancellationToken cancellationToken);

    /// <summary>
    /// Зарегистрировать нового пользователя.
    /// </summary>
    Task<AuthCreateUserResponse> CreateUserAsync(AuthCreateUserRequest request, CancellationToken cancellationToken);

    /// <summary>
    /// Обновить данные пользователя.
    /// </summary>
    Task<AuthUpdateUserResponse> UpdateUserAsync(AuthUpdateUserRequest request, CancellationToken cancellationToken);

    /// <summary>
    /// Получить список ролей пользователей.
    /// </summary>
    Task<AuthGetUsersRolesListResponse> GetUsersRolesListAsync(CancellationToken cancellationToken);
}
