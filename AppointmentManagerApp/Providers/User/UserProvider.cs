﻿using AppointmentManagerApp.Models.Common;
using AppointmentManagerApp.Models.User.Domain;
using AppointmentManagerApp.Models.User.Requests;

namespace AppointmentManagerApp.Providers.User;

/// <inheritdoc cref="IUserProvider"/>
public class UserProvider : ProviderBase, IUserProvider
{
    private readonly IHttpClientFactory _clientFactory;

    public UserProvider(IHttpClientFactory clientFactory)
    {
        _clientFactory = clientFactory;
    }

    public async Task<CreateUserResponse> CreateUserAsync(CreateUserRequest request, CancellationToken cancellationToken)
    {
         var result = await PostAsync<CreateUserRequest, CreateUserResponse>(
            _clientFactory, 
            Constants.USER_HTTP_CLIENT_NAME, 
            request, 
            "user", 
            cancellationToken);

        return result;
    }

    public async Task<UserBase> GetUserAsync(Guid id, CancellationToken cancellationToken)
    {
        return await GetAsync<UserBase>(
            _clientFactory, 
            Constants.USER_HTTP_CLIENT_NAME, 
            $"user/{id}", 
            cancellationToken);
    }

    public async Task<List<UserBase>> GetUsersAsync(CancellationToken cancellationToken)
    {
        return await GetAsync<List<UserBase>>(
            _clientFactory,
            Constants.USER_HTTP_CLIENT_NAME,
            "user/list",
            cancellationToken);
    }
}
