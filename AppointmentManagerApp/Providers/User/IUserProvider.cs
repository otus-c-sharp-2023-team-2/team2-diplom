﻿using AppointmentManagerApp.Models.User.Domain;
using AppointmentManagerApp.Models.User.Requests;

namespace AppointmentManagerApp.Providers.User;

/// <summary>
/// Провайдер пользователей.
/// </summary>
public interface IUserProvider
{
    /// <summary>
    /// Зарегистрировать нового пользователя.
    /// </summary>
    Task<CreateUserResponse> CreateUserAsync(CreateUserRequest request, CancellationToken cancellationToken);

    /// <summary>
    /// Получить пользователя по id.
    /// </summary>
    Task<UserBase> GetUserAsync(Guid id, CancellationToken cancellationToken);

    /// <summary>
    /// Получить пользователей.
    /// </summary>
    Task<List<UserBase>> GetUsersAsync(CancellationToken cancellationToken);
}
