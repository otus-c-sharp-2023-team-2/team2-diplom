﻿using AppointmentManagerApp.Models.Schedule.Service;

namespace AppointmentManagerApp.Providers.Schedule;

public interface IScheduleProvider
{
	Task<GetSchedulesByCustomerResponse> GetSchedulesByCustomerAsync(string guid, CancellationToken cancellationToken);
}