﻿using AppointmentManagerApp.Models.Common;
using AppointmentManagerApp.Models.User;
using AppointmentManagerApp.Models.Schedule.Service;
using System.IO;

namespace AppointmentManagerApp.Providers.Schedule;

public class ScheduleProvider : IScheduleProvider
{
	private readonly IHttpClientFactory _clientFactory;

	public ScheduleProvider(IHttpClientFactory clientFactory, IHttpContextAccessor contextAccessor)
	{
		_clientFactory = clientFactory;
	}

	public async Task<GetSchedulesByCustomerResponse> GetSchedulesByCustomerAsync(string guid, CancellationToken cancellationToken)
	{
		cancellationToken.ThrowIfCancellationRequested();
		var client = _clientFactory.CreateClient(Constants.AUTH_HTTP_CLIENT_NAME);
		try
		{
			using var message = await client.GetAsync($"http://localhost:7778/Schedule/Customer/{guid}", cancellationToken);

			if (!message.IsSuccessStatusCode)
			{
				var errorResult = await message.Content?.ReadFromJsonAsync<ResponseBase>();

				if (string.IsNullOrEmpty(errorResult?.ErrorMessage))
				{
					throw new BadHttpRequestException("Не удалось выполнить запрос.", (int)message.StatusCode);
				}

				return new GetSchedulesByCustomerResponse() { ErrorMessage = errorResult.ErrorMessage };
			}
			var content = await message.Content.ReadAsStringAsync();
			var result = await message.Content?.ReadFromJsonAsync<List<Models.Schedule.Service.Schedule>>();

			return new GetSchedulesByCustomerResponse
			{
				Schedules = result,
			};
		}
		catch
		{
			return new GetSchedulesByCustomerResponse() { ErrorMessage = "Не удалось выполнить запрос." };
		}


		//var result =
		//	await GetAsync<List<Models.Schedule.Service.Schedule>>($"http://localhost:7778/Schedule/Customer/{guid}",
		//		cancellationToken);

		//return await GetAsync<List<Models.Schedule.Service.Schedule>>($"http://localhost:7778/Schedule/Customer/{guid}", cancellationToken);
	}

	
}