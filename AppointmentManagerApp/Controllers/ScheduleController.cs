﻿using AppointmentManagerApp.Models;
using AppointmentManagerApp.Models.Schedule;
using AppointmentManagerApp.Services.Auth;
using AppointmentManagerApp.Services.Schedule;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Claims;
using Microsoft.IdentityModel.JsonWebTokens;

namespace AppointmentManagerApp.Controllers;

public class ScheduleController : Controller
{
	private readonly IScheduleService _scheduleService;

	public ScheduleController(IScheduleService scheduleService)
	{
		_scheduleService = scheduleService;
	}

	[Authorize]
	public IActionResult Index()
	{
		var model = new ScheduleModel
		{
			Schedules = _scheduleService.GetSchedulesByCustomerAsync(HttpContext.User.Claims.FirstOrDefault(x => string.Equals(x.Type, ClaimTypes.NameIdentifier, StringComparison.OrdinalIgnoreCase)
				|| string.Equals(x.Type, JwtRegisteredClaimNames.NameId, StringComparison.OrdinalIgnoreCase))?.Value, CancellationToken.None).Result.Schedules?.Select(s => new Schedule
				{
					EndTime = s.EndTime,
					StartTime = s.StartTime,
				}).ToList(),
		};//_authService.GetUserProfileModel(HttpContext.User.Claims);

		return View(nameof(Index), model);
	}


}
