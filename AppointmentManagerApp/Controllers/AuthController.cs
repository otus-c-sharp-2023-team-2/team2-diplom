﻿using AppointmentManagerApp.Models.Auth.ViewModel;
using AppointmentManagerApp.Services.Auth;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AppointmentManagerApp.Controllers;

public class AuthController : Controller
{
    private readonly IAuthService _authService;
    public AuthController(IAuthService authService)
    {
        _authService = authService;
    }

    [AllowAnonymous]
    public IActionResult Login()
    {
        return View(nameof(Login));
    }

    [AllowAnonymous]
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Login(LoginVm model, CancellationToken cancellationToken)
    {
        if (!ModelState.IsValid)
        {
            return View(nameof(Login));
        }

        var loginSuccess = await _authService.LoginAsync(model.Email, model.Password, cancellationToken);
        if(loginSuccess)
            return RedirectToAction("Index", "Home");

        model.IsLoginFailed = true;

        return View(nameof(Login), model);
    }

    [Authorize]
    public async Task<IActionResult> SignOut(CancellationToken cancellationToken)
    {
        await _authService.SignOutAsync(cancellationToken);
        return RedirectToAction("Index", "Home");
    }
}
