﻿using AppointmentManagerApp.Models;
using AppointmentManagerApp.Models.Auth.Requests;
using AppointmentManagerApp.Models.Benefit.Requests;
using AppointmentManagerApp.Models.Notification.Requests;
using AppointmentManagerApp.Models.User.Domain;
using AppointmentManagerApp.Models.User.Requests;
using AppointmentManagerApp.Models.User.ViewModel;
using AppointmentManagerApp.Providers.Benefit;
using AppointmentManagerApp.Services.Auth;
using AppointmentManagerApp.Services.Notification;
using AppointmentManagerApp.Services.User;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using AppointmentManagerApp.Models.Notification.Requests;

namespace AppointmentManagerApp.Controllers;

public class UserController : Controller
{
    private readonly IAuthService _authService;
    private readonly IUserService _userService;
    private readonly INotificationService _notificationService;
    private readonly IMapper _mapper;
    private readonly IBenefitProvider _benefitProvider;

    public UserController(IUserService userService, IAuthService authService, IMapper mapper, INotificationService notificationService, IBenefitProvider benefitProvider)
    {
        _userService = userService;
        _authService = authService;
        _mapper = mapper;
        _benefitProvider = benefitProvider;
        _notificationService = notificationService;
    }

    [Authorize]
    public async Task<IActionResult> Index()
    {
        var user = await _userService.GetUserAsync();
        var benefit = await _benefitProvider.GetDiscountBenefitAsync(new GetUserBenefitRequest() { UserId = user.Id, Subscription = 3, TrainingSessions = 0 });
        var model = _mapper.Map<UserBase, UserVm>(user);
        model.Discount = benefit?.BenefitUser;

        return View(nameof(Index), model);
    }

    [AllowAnonymous]
    public IActionResult Create()
    {
        return View(nameof(Create));
    }

    [AllowAnonymous]
    [HttpPost]
    public async Task<IActionResult> Create(UserVm model, CancellationToken cancellationToken)
    {
        var authModel = _mapper.Map<UserVm, AuthCreateUserRequest>(model);
        var creationResult = await _authService.CreateUserAsync(authModel, cancellationToken);

        if (!string.IsNullOrEmpty(creationResult.ErrorMessage))
        {
            model.ErrorMessage = $"Не удалось зарегистрировать нового пользователя: {creationResult.ErrorMessage}";
            return View(nameof(Create), model);
        }
        //создаем канал уведомлений по почте зарегистрированного пользователя
        var req = new CreateNotificationChannelRequest()
        {
            Adress = authModel.Email,
            UserId = creationResult.UserId,
            ChannelType = 0
        };
        await _notificationService.CreateNotificationChannelAsync(req, cancellationToken);
        var coreModel = _mapper.Map<UserVm, CreateUserRequest>(model);
        coreModel.Id = creationResult.UserId;

        var res = await _userService.CreateUserAsync(coreModel, cancellationToken);

        await _benefitProvider.ApplyRegitrationAsync(new ApplyRegitrationRequest()
        {
            UserId = creationResult.UserId,
            RefCode = coreModel.Promocode,
            UserCode = Guid.NewGuid().ToString()
        }
        );


        return View("~/Views/Home/Index.cshtml", new IndexModel() { SuccessMessage = "Вы успешно зарегистрированы. Теперь вы можете войти." });
    }
}
