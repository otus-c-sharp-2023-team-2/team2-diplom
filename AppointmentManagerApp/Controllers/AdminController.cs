﻿using AppointmentManagerApp.Models.Admin.ViewModel;
using AppointmentManagerApp.Models.Auth.Domain;
using AppointmentManagerApp.Models.Auth.Requests;
using AppointmentManagerApp.Models.User.Domain;
using AppointmentManagerApp.Models.User.Requests;
using AppointmentManagerApp.Services.Auth;
using AppointmentManagerApp.Services.User;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AppointmentManagerApp.Controllers;

[Authorize(Roles = nameof(Roles.Admin))]
public class AdminController : Controller
{
    private readonly IAuthService _authService;
    private readonly IUserService _userService;
    private readonly IMapper _mapper;

    public AdminController(IUserService userService, IAuthService authService, IMapper mapper)
    {
        _userService = userService;
        _authService = authService;
        _mapper = mapper;
    }

    public IActionResult Index()
    {
        return View(nameof(Index));
    }

    public async Task<IActionResult> UserManagement(CancellationToken cancellationToken)
    {
        var allUsersTask = _userService.GetUsersListAsync();
        var userRolesTask = _authService.GetUsersRolesListAsync(cancellationToken);

        await Task.WhenAll(allUsersTask, userRolesTask);

        var castedUsers = allUsersTask.Result
            .Select(item => new UserFullInfo()
            { 
                Id = item.Id,
                Email = item.Email,
                FirstName = item.FirstName,
                LastName = item.LastName,
                PhoneNumber = item.PhoneNumber,
                Promocode = item.Promocode,
                Role = userRolesTask.Result.FirstOrDefault(x => x.Id == item.Id)?
                .Claims?.FirstOrDefault()?.Value
            });

        return View(nameof(UserManagement), new UserManagementVm { 
            Users = castedUsers
        });
    }

    [HttpPost]
    public async Task<IActionResult> CreateUser(CreateUserByAdminVm model, CancellationToken cancellationToken)
    {
        var authModel = _mapper.Map<CreateUserByAdminVm, AuthCreateUserRequest>(model);
        var creationResult = await _authService.CreateRandomPasswordUserAsync(authModel, cancellationToken);

        if (!string.IsNullOrEmpty(creationResult.ErrorMessage))
        {
            model.ErrorMessage = $"Не удалось зарегистрировать нового пользователя: {creationResult.ErrorMessage}";
            return View("~/Views/Admin/UserManagement.cshtml", new UserManagementVm() { ErrorMessage = model.ErrorMessage });
        }

        var coreModel = _mapper.Map<CreateUserByAdminVm, CreateUserRequest>(model);
        coreModel.Id = creationResult.UserId;

        await _userService.CreateUserAsync(coreModel, cancellationToken);

        return View("~/Views/Admin/UserManagement.cshtml", new UserManagementVm() { SuccessMessage = "Пользователь успешно зарегистрирован." });
    }
}
