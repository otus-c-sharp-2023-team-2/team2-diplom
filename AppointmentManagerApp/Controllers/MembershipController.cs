﻿using AppointmentManagerApp.Models;
using AppointmentManagerApp.Models.Admin.ViewModel;
using AppointmentManagerApp.Models.Membership.Domain;
using AppointmentManagerApp.Models.Membership.ViewModel;
using AppointmentManagerApp.Models.User.Domain;
using AppointmentManagerApp.Models.User.ViewModel;
using AppointmentManagerApp.Services.Membership;
using AppointmentManagerApp.Services.User;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Runtime.Serialization;

namespace AppointmentManagerApp.Controllers;

public class MembershipController : Controller
{
    private readonly IMembershipService _membershipService;
    private readonly IUserService _userService;
    private readonly IMapper _mapper;

    public MembershipController(IMembershipService membershipService,
        IUserService userService,
        IMapper mapper)
    {
        _membershipService = membershipService;
        _userService = userService;
        _mapper = mapper;
    }

    [HttpGet]
    public async Task Index()
    {
        string content = @"<form method='post'>
                <label>Введите номер Абонемента:</label><br />
                
                <input type='number' name='aaa' /><br />
                
                <input type='submit' value='Получить' />
            </form>";
        Response.ContentType = "text/html;charset=utf-8";
        await Response.WriteAsync(content);
    }

    [HttpPost]
    public async Task<IActionResult> CreateMembership(MembershipViewModel model)
    {
        var user = await _userService.GetUserAsync();
        var coreModel = _mapper.Map<MembershipViewModel, Membership>(model);
        coreModel.UserId = user.Id;
        
        await _membershipService.CreateUserMembership(coreModel);

        return View("~/Views/Home/Index.cshtml", new IndexModel() { SuccessMessage = "Абонемент успешно приобретен." });
    }
}

