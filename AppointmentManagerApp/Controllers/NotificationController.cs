﻿using AppointmentManagerApp.Models.Notification.ViewModel;
using AppointmentManagerApp.Models.Schedule;
using AppointmentManagerApp.Services.Notification;
using AppointmentManagerApp.Services.Schedule;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Claims;
using Microsoft.IdentityModel.JsonWebTokens;

namespace AppointmentManagerApp.Controllers
{
	public class NotificationController : Controller
	{
		private readonly INotificationService _notificationService;

		public NotificationController(INotificationService notificationService)
		{
			_notificationService = notificationService;
		}

		[Authorize]
		public async Task<IActionResult> Index()
		{
			var channels = await _notificationService.GetNotificationChannelAsync();
			if (channels!=null) 
			{
				var model = new NotificationViewModel();
				model.Channels = new List<string>();
				foreach (var ch in channels.Channels)
				{
					model.Channels.Add(ch.Address);
				}
				ViewBag.Notifications = model;
			}			
			//return View(nameof(Index), model);
			return View();
		}
		public async Task<IActionResult> SendTestMessage(CancellationToken cancellationToken)
		{
			await _notificationService.CreateNotificationAsync("Пробное сообщение",cancellationToken);
			return RedirectToAction("Index", "Notification");
		}
	}
}
