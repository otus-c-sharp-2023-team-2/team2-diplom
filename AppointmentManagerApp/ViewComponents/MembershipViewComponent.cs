﻿using AppointmentManagerApp.Models.Membership.ViewModel;
using AppointmentManagerApp.Services.Membership;
using AppointmentManagerApp.Services.User;
using Microsoft.AspNetCore.Mvc;

namespace AppointmentManagerApp.ViewComponents;

[ViewComponent]
public class MembershipViewComponent : ViewComponent
{
    private readonly IMembershipService _membershipService;
    private readonly IUserService _userService;

    public MembershipViewComponent(IMembershipService membershipService,
        IUserService userService)
    {
        _membershipService = membershipService;
        _userService = userService;
    }

    public async Task<IViewComponentResult> InvokeAsync()
    {
        var user = await _userService.GetUserAsync();
        var memberships = await _membershipService.GetUserMembership(user.Id);

        var model = new UserMembershipsViewModel();
        model.UserId = user.Id;
        model.Memberships = memberships.Select(x => new MembershipViewModel()
        {
            Type = x.Type,
            Duration = x.Duration,
            StartDate = x.StartDate.ToString("dd.MM.yyyy"),
            EndDate = x.StartDate.AddDays(x.Duration).ToString("dd.MM.yyyy"),
        });

        return View("~/Views/Membership/_UserMemberships.cshtml", model);
    }
}

