﻿using AppointmentManagerApp.Middlewares;

namespace AppointmentManagerApp.Extensions;

/// <summary>
/// Расширения для middleware.
/// </summary>
public static class MiddlewareExtensions
{
    /// <summary>
    /// Подключение middleware с DI активатором.
    /// </summary>
    /// <param name="app"></param>
    /// <returns></returns>
    public static IApplicationBuilder UseFactoryActivatedMiddleware(
       this IApplicationBuilder app)
            => app.UseMiddleware<JwtTokenMiddleware>();
}
