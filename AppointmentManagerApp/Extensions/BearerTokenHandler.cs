﻿using Microsoft.AspNetCore.Authentication;

namespace AppointmentManagerApp.Extensions;

public class BearerTokenHandler : DelegatingHandler
{
    private readonly IHttpContextAccessor _contextAccessor;
    public BearerTokenHandler(IHttpContextAccessor contextAccessor)
    {
        _contextAccessor = contextAccessor;
    }

    protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
    {        
        var token = await _contextAccessor?.HttpContext?.GetTokenAsync("Bearer", "access_token");
        if (!request.Headers.Contains("Authorization") && token is not null)
        {
            request.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
        }

        return await base.SendAsync(request, cancellationToken);
    }
}