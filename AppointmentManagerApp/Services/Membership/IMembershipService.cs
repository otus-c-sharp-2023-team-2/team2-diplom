﻿using AppointmentManagerApp.Models.Membership.Domain;

namespace AppointmentManagerApp.Services.Membership;

public interface IMembershipService
{
    Task<IEnumerable<Models.Membership.Domain.Membership>> GetUserMembership(Guid userId);

    Task CreateUserMembership(Models.Membership.Domain.Membership model);
}
