﻿using AppointmentManagerApp.Models.Common;
using AppointmentManagerApp.Models.Membership.Domain;
using AppointmentManagerApp.Models.Membership.ViewModel;
using Microsoft.Extensions.Options;

namespace AppointmentManagerApp.Services.Membership;

public class MembershipService : IMembershipService
{
    private readonly IOptions<AppointmentManagerAppSettings> _settings;

    public MembershipService(IOptions<AppointmentManagerAppSettings> settings)
    {
        _settings = settings;
    }

    public async Task<IEnumerable<Models.Membership.Domain.Membership>> GetUserMembership(Guid userId)
    {
        using (var client = new HttpClient())
        {
            var response = await client.GetAsync($"{_settings.Value.MembershipServiceUrl}/Membership/" + userId.ToString());
            var result = await response.Content.ReadFromJsonAsync<IEnumerable<Models.Membership.Domain.Membership>>();

            return result;
        }
    }

    public async Task CreateUserMembership(Models.Membership.Domain.Membership model)
    {
        using (var client = new HttpClient())
        {
            var response = await client.PostAsJsonAsync<Models.Membership.Domain.Membership>($"{_settings.Value.MembershipServiceUrl}/Membership", model);
            
            var result = response.Content.ReadAsStringAsync();
            return;
        }
    }
}
