﻿using System.Security.Claims;
using AppointmentManagerApp.Models.User.Domain;
using AppointmentManagerApp.Models.User.Requests;

namespace AppointmentManagerApp.Services.User;

/// <summary>
/// Сервис пользователя.
/// </summary>
public interface IUserService
{    
    /// <summary>
    /// Зарегистрировать нового пользователя.
    /// </summary>
    Task<CreateUserResponse> CreateUserAsync(CreateUserRequest request, CancellationToken cancellationToken);

    /// <summary>
    /// Получить модель пользователя.
    /// </summary>
    Task<UserBase> GetUserAsync(CancellationToken cancellationToken = default);

    /// <summary>
    /// Получить список пользователей.
    /// </summary>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    Task<IEnumerable<UserBase>> GetUsersListAsync(CancellationToken cancellationToken = default);
}
