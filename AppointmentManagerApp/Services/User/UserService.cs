﻿using AppointmentManagerApp.Models.User.Domain;
using AppointmentManagerApp.Models.User.Requests;
using AppointmentManagerApp.Providers.User;
using AppointmentManagerApp.Services.Auth;

namespace AppointmentManagerApp.Services.User;

/// <inheritdoc cref="IUserService"/>
public class UserService : IUserService
{
    private readonly IAuthService _authService;
    private readonly IUserProvider _userProvider;
    private readonly IHttpContextAccessor _contextAccessor;

    public UserService(IAuthService authService, IUserProvider userProvider, IHttpContextAccessor contextAccessor)
    {
        _authService = authService;
        _userProvider = userProvider;
        _contextAccessor = contextAccessor;
    }

    public async Task<CreateUserResponse> CreateUserAsync(CreateUserRequest request, CancellationToken cancellationToken)
    {
        return await _userProvider.CreateUserAsync(request, cancellationToken);
    }

    public async Task<UserBase> GetUserAsync(CancellationToken cancellationToken = default)
    {
        var userContext = _authService.GetUserFromToken(_contextAccessor?.HttpContext?.User?.Claims);

        if (userContext == null)
        {
            return default(UserBase);
        }

        var user = await _userProvider.GetUserAsync(userContext.Id, cancellationToken);

        return user;
    }

    public async Task<IEnumerable<UserBase>> GetUsersListAsync(CancellationToken cancellationToken = default)
    {
        var users = await _userProvider.GetUsersAsync(cancellationToken);

        return users;
    }
}
