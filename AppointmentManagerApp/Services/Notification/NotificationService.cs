﻿using AppointmentManagerApp.Models.Notification.Requests;
using AppointmentManagerApp.Models.User.Domain;
using AppointmentManagerApp.Models.User.Requests;
using AppointmentManagerApp.Providers.Notification;
using AppointmentManagerApp.Services.Auth;

namespace AppointmentManagerApp.Services.Notification
{
    /// <inheritdoc cref="INotificationService"/>
    public class NotificationService : INotificationService
    {
        private readonly IHttpContextAccessor _contextAccessor;
        private readonly INotificationProvider _notificationProvider;
        private readonly IAuthService _authService;
        public NotificationService(IHttpContextAccessor contextAccessor, INotificationProvider notificationProvider, IAuthService authService) 
        {
            _contextAccessor = contextAccessor;
            _notificationProvider = notificationProvider;
            _authService = authService;
        }
        public async Task<CreateNotificationChannelResponse> CreateNotificationChannelAsync(CreateNotificationChannelRequest request, CancellationToken cancellationToken)
        {

            return await _notificationProvider.CreateNotificationChannelAsync(request, cancellationToken);
        }
        public async Task<NotificationChannelResponse> GetNotificationChannelAsync(CancellationToken cancellationToken)
        {
			var userContext = _authService.GetUserFromToken(_contextAccessor?.HttpContext?.User?.Claims);

			if (userContext == null)
			{
				//return default(UserBase);
			}

			var channels = await _notificationProvider.GetNotificationChannelsAsync(userContext.Id, cancellationToken);

			return channels;
		}
        public async Task<bool> CreateNotificationAsync(string message, CancellationToken token)
        {
            var userContext = _authService.GetUserFromToken(_contextAccessor?.HttpContext?.User?.Claims);
            if (userContext == null) 
            {
                return false;
            }
            var resp = _notificationProvider.CreateNotification(userContext.Id, message, token);

            return true;
        }
    }
}
