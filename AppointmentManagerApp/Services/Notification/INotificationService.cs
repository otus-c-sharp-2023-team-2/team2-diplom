﻿using AppointmentManagerApp.Models.Notification.Requests;
using AppointmentManagerApp.Models.User.Requests;

namespace AppointmentManagerApp.Services.Notification
{
    /// <summary>
    /// сервис уведомлений
    /// </summary>
    public interface INotificationService
    {
        /// <summary>
        /// Создать новый канал уведомлений
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<CreateNotificationChannelResponse> CreateNotificationChannelAsync(CreateNotificationChannelRequest request, CancellationToken cancellationToken = default);
        Task<NotificationChannelResponse> GetNotificationChannelAsync(CancellationToken cancellationToken = default);
		Task<bool> CreateNotificationAsync(string message, CancellationToken cancelToken = default);

	}
}
