﻿using AppointmentManagerApp.Models.Schedule.Service;

namespace AppointmentManagerApp.Services.Schedule
{
	public interface IScheduleService
	{
		Task<GetSchedulesByCustomerResponse> GetSchedulesByCustomerAsync(string guid, CancellationToken cancellationToken);
	}
}
