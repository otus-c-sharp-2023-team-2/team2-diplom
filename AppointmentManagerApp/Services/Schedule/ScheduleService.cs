﻿using AppointmentManagerApp.Models.Schedule.Service;
using AppointmentManagerApp.Providers.Schedule;

namespace AppointmentManagerApp.Services.Schedule
{
	public class ScheduleService : IScheduleService
	{
		private readonly IScheduleProvider _scheduleProvider;

		public ScheduleService(IScheduleProvider scheduleProvider)
		{
			_scheduleProvider = scheduleProvider;
		}

		public async Task<GetSchedulesByCustomerResponse> GetSchedulesByCustomerAsync(string guid, CancellationToken cancellationToken)
		{
			return await _scheduleProvider.GetSchedulesByCustomerAsync(guid, cancellationToken);
		}
	}
}
