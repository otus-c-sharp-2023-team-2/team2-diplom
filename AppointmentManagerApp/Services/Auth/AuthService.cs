﻿using AppointmentManagerApp.Models.Auth;
using AppointmentManagerApp.Models.Auth.Requests;
using AppointmentManagerApp.Models.Common;
using AppointmentManagerApp.Models.User.Domain;
using AppointmentManagerApp.Providers.Auth;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;

namespace AppointmentManagerApp.Services.Auth;

/// <inheritdoc cref="IAuthService"/>
public class AuthService : IAuthService
{
    private readonly IAuthProvider _authProvider;
    private readonly IHttpContextAccessor _contextAccessor;
    private readonly AccessTokenSettings _settings;

    private static Random random = new Random();

    public AuthService(IAuthProvider authProvider, IHttpContextAccessor contextAccessor, IOptions<AccessTokenSettings> settings)
    {
        _authProvider = authProvider;
        _contextAccessor = contextAccessor;
        _settings = settings?.Value ?? throw new ArgumentNullException(nameof(settings));
    }

    public async Task<bool> LoginAsync(string username, string password, CancellationToken cancellationToken)
    {
        var requestModel = new LoginRequest() { Email = username, Password = password };
        var result = await _authProvider.LoginAsync(requestModel, cancellationToken);

        if (result is null || !string.IsNullOrEmpty(result.ErrorMessage))
            return false;

        if (!string.IsNullOrEmpty(result.AccessToken))
            _contextAccessor.HttpContext?.Response.Cookies.Append(AuthConstants.JWT_TOKEN_COOKIE_NAME, result.AccessToken);

        if (!string.IsNullOrEmpty(result.RefreshToken))
            _contextAccessor.HttpContext?.Response.Cookies.Append(AuthConstants.REFRESH_TOKEN_COOKIE_NAME, result.RefreshToken);

        return true;
    }

    public async Task<RefreshTokenResponse> RefreshTokenAsync(string accessToken, string refreshToken, CancellationToken cancellationToken)
    {
        var requestModel = new RefreshTokenRequest() { AccessToken = accessToken, RefreshToken = refreshToken };
        var result = await _authProvider.RefreshTokenAsync(requestModel, cancellationToken);

        if (result is null)
            return null;

        if (!string.IsNullOrEmpty(result.AccessToken))
            _contextAccessor.HttpContext?.Response.Cookies.Append(AuthConstants.JWT_TOKEN_COOKIE_NAME, result.AccessToken);

        if (!string.IsNullOrEmpty(result.RefreshToken))
            _contextAccessor.HttpContext?.Response.Cookies.Append(AuthConstants.REFRESH_TOKEN_COOKIE_NAME, result.RefreshToken);

        return result;
    }

    public async Task<bool> SignOutAsync(CancellationToken cancellationToken)
    {
        var jwtToken = _contextAccessor?.HttpContext?.Request?.Cookies[AuthConstants.JWT_TOKEN_COOKIE_NAME];

        if (jwtToken == null)
        {
            _contextAccessor.HttpContext?.Response?.Cookies.Delete(AuthConstants.JWT_TOKEN_COOKIE_NAME);
            _contextAccessor.HttpContext?.Response?.Cookies.Delete(AuthConstants.REFRESH_TOKEN_COOKIE_NAME);
            return true;
        }

        var parsedToken = this.ParseJwtToken(jwtToken);

        var userId = parsedToken.Claims?.FirstOrDefault(x => string.Equals(x.ValueType, ClaimTypes.NameIdentifier, StringComparison.OrdinalIgnoreCase)
        || string.Equals(x.Type, JwtRegisteredClaimNames.NameId, StringComparison.OrdinalIgnoreCase))?.Value;
        if (userId is null)
            return false;

        var requestModel = new SignOutRequest() { UserId = Guid.Parse(userId) };
        var result = await _authProvider.SignOutAsync(requestModel, cancellationToken);

        _contextAccessor.HttpContext?.Response.Cookies.Delete(AuthConstants.JWT_TOKEN_COOKIE_NAME);
        _contextAccessor.HttpContext?.Response.Cookies.Delete(AuthConstants.REFRESH_TOKEN_COOKIE_NAME);

        return true;
    }

    public async Task<AuthCreateUserResponse> CreateUserAsync(AuthCreateUserRequest request, CancellationToken cancellationToken)
    {
        return await _authProvider.CreateUserAsync(request, cancellationToken);
    }

    public async Task<AuthCreateUserResponse> CreateRandomPasswordUserAsync(AuthCreateUserRequest request, CancellationToken cancellationToken)
    {
        int length = 8;
        const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789&?%$@";
        request.Password = new string(Enumerable.Repeat(chars, length)
          .Select(s => s[random.Next(s.Length)]).ToArray());

        return await _authProvider.CreateUserAsync(request, cancellationToken);
    }

    public JwtSecurityToken ParseJwtToken(string jwtToken)
    {
        RSA rsa = RSA.Create();
        rsa.ImportRSAPublicKey(
            source: Convert.FromBase64String(_settings.PublicKey),
            bytesRead: out int _
        );

        var rsaKey = new RsaSecurityKey(rsa);

        var tokenHandler = new JwtSecurityTokenHandler();

        tokenHandler.ValidateToken(jwtToken, new TokenValidationParameters
        {
            ValidateIssuerSigningKey = true,
            IssuerSigningKey = rsaKey,
            ValidateIssuer = false,
            ValidateAudience = false,
            ValidateLifetime = false,
            ClockSkew = _settings.JwtTokenClockSkew
        }, out SecurityToken validatedToken);

        return (JwtSecurityToken)validatedToken;
    }

    public AuthUser GetUserFromToken(IEnumerable<Claim> claims)
    {
        string id = claims.FirstOrDefault(x => string.Equals(x.Type, ClaimTypes.NameIdentifier, StringComparison.OrdinalIgnoreCase)
        || string.Equals(x.Type, JwtRegisteredClaimNames.NameId, StringComparison.OrdinalIgnoreCase))?.Value;

        return new AuthUser()
        {
            Id = !string.IsNullOrEmpty(id) && Guid.TryParse(id, out Guid result) ? result : Guid.Empty,

            Email = claims.FirstOrDefault(x => string.Equals(x.Type, ClaimTypes.Email, StringComparison.OrdinalIgnoreCase)
        || string.Equals(x.Type, JwtRegisteredClaimNames.Email, StringComparison.OrdinalIgnoreCase))?.Value ?? string.Empty,

            FirstName = claims.FirstOrDefault(x => string.Equals(x.Type, ClaimTypes.Name, StringComparison.OrdinalIgnoreCase)
        || string.Equals(x.Type, JwtRegisteredClaimNames.Name, StringComparison.OrdinalIgnoreCase))?.Value ?? string.Empty,

            LastName = claims.FirstOrDefault(x => string.Equals(x.Type, ClaimTypes.Surname, StringComparison.OrdinalIgnoreCase)
        || string.Equals(x.Type, JwtRegisteredClaimNames.FamilyName, StringComparison.OrdinalIgnoreCase))?.Value ?? string.Empty
        };
    }

    public async Task<IEnumerable<AuthUser>> GetUsersRolesListAsync(CancellationToken cancellationToken)
    {
        var response = await _authProvider.GetUsersRolesListAsync(cancellationToken);
        return response.Users;
    }
}
