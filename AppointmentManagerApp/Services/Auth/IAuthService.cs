﻿using AppointmentManagerApp.Models.Auth.Requests;
using AppointmentManagerApp.Models.User;
using AppointmentManagerApp.Models.User.Domain;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace AppointmentManagerApp.Services.Auth;

/// <summary>
/// Сервис авторизации.
/// </summary>
public interface IAuthService
{
    /// <summary>
    /// Залогинить пользователя.
    /// </summary>
    Task<bool> LoginAsync(string username, string password, CancellationToken cancellationToken);

    /// <summary>
    /// Обновить токены.
    /// </summary>
    Task<RefreshTokenResponse> RefreshTokenAsync(string accessToken, string refreshToken, CancellationToken cancellationToken);

    /// <summary>
    /// Разлогинить пользователя.
    /// </summary>
    Task<bool> SignOutAsync(CancellationToken cancellationToken);

    /// <summary>
    /// Зарегистрировать нового пользователя.
    /// </summary>
    Task<AuthCreateUserResponse> CreateUserAsync(AuthCreateUserRequest request, CancellationToken cancellationToken);

    /// <summary>
    /// Зарегистрировать нового пользователя с сгенерированным паролем.
    /// </summary>
    Task<AuthCreateUserResponse> CreateRandomPasswordUserAsync(AuthCreateUserRequest request, CancellationToken cancellationToken);

    /// <summary>
    /// Получить значение токена.
    /// </summary>
    JwtSecurityToken ParseJwtToken(string token);

    /// <summary>
    /// Получить модель из контекста пользователя.
    /// </summary>
    AuthUser GetUserFromToken(IEnumerable<Claim> claims);

    /// <summary>
    /// Получить список ролей пользователей.
    /// </summary>
    Task<IEnumerable<AuthUser>> GetUsersRolesListAsync(CancellationToken cancellationToken);
}
