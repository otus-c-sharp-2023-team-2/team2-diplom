﻿namespace AppointmentManagerApp.Models
{
    public class IndexModel
    {
        public string AppTitle { get; set; }

        public string SuccessMessage { get; set; }

        public string ErrorMessage { get; set; }
    }
}
