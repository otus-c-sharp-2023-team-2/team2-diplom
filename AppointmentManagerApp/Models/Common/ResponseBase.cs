﻿namespace AppointmentManagerApp.Models.Common;

public class ResponseBase
{
    public string ErrorMessage { get; set; }
}
