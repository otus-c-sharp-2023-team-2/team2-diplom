﻿namespace AppointmentManagerApp.Models.Common;

public class AppointmentManagerAppSettings
{
    public string UserServiceUrl { get; set; }
    public string NotificationUrl { get; set; }
    public string BenefitServiceUrl { get; set; }
    public string MembershipServiceUrl { get; set; }
}
