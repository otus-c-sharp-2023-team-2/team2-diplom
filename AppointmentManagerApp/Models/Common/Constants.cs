﻿namespace AppointmentManagerApp.Models.Common;

public class Constants
{
    /// <summary>
    /// Название http клиента провайдера авторизации.
    /// </summary>
    public const string AUTH_HTTP_CLIENT_NAME = "AUTH_HTTP_CLIENT";

    /// <summary>
    /// Название http клиента сервиса по скидкам.
    /// </summary>
    public const string BENEFIT_HTTP_CLIENT_NAME = "BENEFIT_HTTP_CLIENT_NAME";

    /// <summary>
    /// Название http клиента провайдера пользователей.
    /// </summary>
    public const string USER_HTTP_CLIENT_NAME = "USER_HTTP_CLIENT";
    /// <summary>
    /// Название http клиента провайдера уведомлений.
    /// </summary>
    public const string Notification_HTTP_CLIENT_NAME = "NOTIFICATION_HTTP_CLIENT";
}
