﻿namespace AppointmentManagerApp.Models.Common;

/// <summary>
/// Настройки access token.
/// </summary>
public class AccessTokenSettings
{
    public string Issuer { get; set; }
    public string Audience { get; set; }
    public string PublicKey { get; set; }
    public string PrivateKey { get; set; }

    /// <summary>
    /// Время доверия к токену.
    /// </summary>
    public TimeSpan JwtTokenClockSkew { get; set; }
}
