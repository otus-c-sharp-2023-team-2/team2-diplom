﻿using System.ComponentModel.DataAnnotations;

namespace AppointmentManagerApp.Models.User.ViewModel;

public class UserVm
{
    [Required(ErrorMessage = "Обязательное поле")]
    [Display(Name = "Электронная почта")]
    [RegularExpression("^\\S+@\\S+\\.\\S+$", ErrorMessage = "Неверный формат Email.")]
    public string Email { get; set; }

    [Required(ErrorMessage = "Обязательное поле")]
    [Display(Name = "Пароль")]
    public string Password { get; set; }

    [Display(Name = "Имя")]
    [MaxLength(100)]
    public string FirstName { get; set; }

    [Display(Name = "Фамилия")]
    [MaxLength(100)]
    public string LastName { get; set; }

    [Display(Name = "Мобильный")]
    public long PhoneNumber { get; set; }

    [Display(Name = "Промокод")]
    public string Promocode { get; set; }

    [Display(Name = "Ваша скидка")]
    public int? Discount { get; set; }

    // public IList<Claim> Claims { get; set; }

    public bool? IsCreationSucceed { get; set; }

    public string ErrorMessage { get; set; } = string.Empty;
}
