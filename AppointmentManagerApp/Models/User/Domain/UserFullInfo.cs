﻿using AppointmentManagerApp.Models.Auth.Domain;

namespace AppointmentManagerApp.Models.User.Domain;

public class UserFullInfo : UserBase
{
    public string Role { get; set; }
}
