﻿using AppointmentManagerApp.Models.Common;

namespace AppointmentManagerApp.Models.User.Requests;

public class CreateUserResponse : ResponseBase
{
    public Guid UserId { get; set; }
}
