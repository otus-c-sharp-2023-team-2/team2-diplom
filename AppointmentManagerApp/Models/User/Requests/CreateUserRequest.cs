﻿namespace AppointmentManagerApp.Models.User.Requests;

public class CreateUserRequest
{
    public Guid Id { get; set; }

    public string Email { get; set; }

    public string FirstName { get; set; }

    public string LastName { get; set; }

    //public IList<Claim> Claims { get; set; }

    public long PhoneNumber { get; set; }

    public string Promocode { get; set; }
}
