﻿namespace AppointmentManagerApp.Models.Benefit.Requests
{
    public class GetDiscountRequest
    {
        /// <summary>
        /// Id клиента в системе
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// Количество купленных тренировок
        /// </summary>
        public int? TrainingSessions { get; set; }

        /// <summary>
        /// Тип абонемента 3/6/12
        /// </summary>
        public int? Subscription { get; set; }
    }
}
