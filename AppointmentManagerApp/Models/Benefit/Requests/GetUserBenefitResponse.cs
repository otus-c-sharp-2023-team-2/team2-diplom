﻿namespace AppointmentManagerApp.Models.Benefit.Requests
{
    public class GetUserBenefitResponse
    {
        public int? BenefitUser { get; set; }
    }
}
