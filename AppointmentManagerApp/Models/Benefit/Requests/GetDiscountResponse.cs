﻿using AppointmentManagerApp.Models.Common;

namespace AppointmentManagerApp.Models.Benefit.Requests
{
    public class GetDiscountResponse : ResponseBase 
    {
        public string discount { get; set; }
    }
}
