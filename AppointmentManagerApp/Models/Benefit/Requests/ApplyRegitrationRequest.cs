﻿namespace AppointmentManagerApp.Models.Benefit.Requests
{
    public class ApplyRegitrationRequest
    {
        /// <summary>
        /// Id клиента в системе
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// Личный Реферальный код клиента
        /// </summary>
        public string UserCode { get; set; } = string.Empty;

        /// <summary>
        /// Реферальный код пригласившего клиента
        /// </summary>
        public string? RefCode { get; set; }
    }
}
