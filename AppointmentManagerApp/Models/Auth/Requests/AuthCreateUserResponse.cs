﻿using AppointmentManagerApp.Models.Common;

namespace AppointmentManagerApp.Models.Auth.Requests;

public class AuthCreateUserResponse : ResponseBase
{
    public Guid UserId { get; set; }
}
