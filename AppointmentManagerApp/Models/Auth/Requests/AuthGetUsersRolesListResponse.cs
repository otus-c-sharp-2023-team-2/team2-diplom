﻿using AppointmentManagerApp.Models.Auth.Domain;
using AppointmentManagerApp.Models.Common;
using AppointmentManagerApp.Models.User.Domain;

namespace AppointmentManagerApp.Models.Auth.Requests;

public class AuthGetUsersRolesListResponse : ResponseBase
{
    public IEnumerable<AuthUser> Users { get; set; }    
}
