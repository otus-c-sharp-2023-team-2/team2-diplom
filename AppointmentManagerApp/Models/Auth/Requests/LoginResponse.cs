﻿using AppointmentManagerApp.Models.Common;

namespace AppointmentManagerApp.Models.Auth.Requests;

public class LoginResponse : ResponseBase
{
    public string IdToken { get; set; }
    public string AccessToken { get; set; }
    public string RefreshToken { get; set; }
}
