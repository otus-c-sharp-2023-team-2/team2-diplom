﻿namespace AppointmentManagerApp.Models.Auth.Requests;

public class SignOutRequest
{
    public Guid UserId { get; set; }
}
