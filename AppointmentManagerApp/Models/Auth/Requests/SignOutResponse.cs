﻿using AppointmentManagerApp.Models.Common;

namespace AppointmentManagerApp.Models.Auth.Requests;

public class SignOutResponse : ResponseBase
{
    public string Message { get; set; }
}
