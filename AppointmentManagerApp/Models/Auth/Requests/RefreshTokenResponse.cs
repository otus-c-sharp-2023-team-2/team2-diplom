﻿using AppointmentManagerApp.Models.Common;

namespace AppointmentManagerApp.Models.Auth.Requests;

public class RefreshTokenResponse : ResponseBase
{
    public string AccessToken { get; set; }
    public string RefreshToken { get; set; }
    public DateTime RefreshTokenExpirationDate { get; set; }
}
