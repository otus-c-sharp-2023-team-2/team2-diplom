﻿using AppointmentManagerApp.Models.Auth.Domain;

namespace AppointmentManagerApp.Models.Auth.Requests;

public class AuthUpdateUserRequest
{
    public string Email { get; set; }

    public string Password { get; set; }

    public string Name { get; set; }

    public string LastName { get; set; }

    public IList<RoleClaim> Claims { get; set; }
}
