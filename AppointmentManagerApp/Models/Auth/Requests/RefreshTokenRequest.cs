﻿namespace AppointmentManagerApp.Models.Auth.Requests;

public class RefreshTokenRequest
{
    public string AccessToken { get; set; }
    public string RefreshToken { get; set; }
}
