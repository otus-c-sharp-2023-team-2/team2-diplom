﻿using AppointmentManagerApp.Models.Auth.Domain;

namespace AppointmentManagerApp.Models.User.Domain;

public class AuthUser
{
    public Guid Id { get; set; }

    public string Email { get; set; }

    public string FirstName { get; set; }

    public string LastName { get; set; }

    public IEnumerable<RoleClaim> Claims { get; set; }
}
