﻿using System.Security.Claims;

namespace AppointmentManagerApp.Models.Auth.Domain;

public class RoleClaim
{
    public string Type { get; set; }

    public string Value { get; set; }
}
