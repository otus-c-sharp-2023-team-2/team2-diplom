﻿namespace AppointmentManagerApp.Models.Auth;

public class AuthConstants
{
    /// <summary>
    /// Название куки c JWT токеном авторизации.
    /// </summary>
    public const string JWT_TOKEN_COOKIE_NAME = "jwt-token";

    /// <summary>
    /// Название куки c Refresh токеном авторизации.
    /// </summary>
    public const string REFRESH_TOKEN_COOKIE_NAME = "refresh-token";
}
