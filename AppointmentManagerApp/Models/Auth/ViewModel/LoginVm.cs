﻿using System.ComponentModel.DataAnnotations;

namespace AppointmentManagerApp.Models.Auth.ViewModel;

public class LoginVm
{
    [Display(Name = "Email")]
    [Required(ErrorMessage = "Email не заполнен")]
    public string Email { get; set; }

    [Display(Name = "Пароль")]
    [Required(ErrorMessage = "Пароль не заполнен")]
    public string Password { get; set; }

    public bool IsLoginFailed { get; set; } = false;
}
