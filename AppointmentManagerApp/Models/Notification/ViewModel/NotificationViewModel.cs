﻿namespace AppointmentManagerApp.Models.Notification.ViewModel
{
	public class NotificationViewModel
	{
		public List<string> Channels { get; set; }
	}
}
