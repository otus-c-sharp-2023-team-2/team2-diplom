﻿
using AppointmentManagerApp.Models.Common;

namespace AppointmentManagerApp.Models.Notification.Requests
{
	public class NotificationChannelResponse : ResponseBase
	{
		public IEnumerable<NotificationChannel> Channels { get; set; }
	}
	public class NotificationChannel
	{
		public Guid Id { get; set; }
		public Guid UserId { get; set; }
		public int ChannelType { get; set; }
		public string Address { get; set; }
	}
}
