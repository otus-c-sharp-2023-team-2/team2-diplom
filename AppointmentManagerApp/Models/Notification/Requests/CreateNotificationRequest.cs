﻿namespace AppointmentManagerApp.Models.Notification.Requests
{
	public class CreateNotificationRequest
	{
		public Guid UserID { get; set; }
		public string Body { get; set; }
		public DateTime SendingDateTime { get; set; }
	}
}
