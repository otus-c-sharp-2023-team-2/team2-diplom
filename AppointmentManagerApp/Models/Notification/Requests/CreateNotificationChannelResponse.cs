﻿using AppointmentManagerApp.Models.Common;

namespace AppointmentManagerApp.Models.Notification.Requests
{
    public class CreateNotificationChannelResponse : ResponseBase
    {
        public Guid NotificationChannelId { get; set; }
    }
}
