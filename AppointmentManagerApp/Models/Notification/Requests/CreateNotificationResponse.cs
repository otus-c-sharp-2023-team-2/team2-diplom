﻿using AppointmentManagerApp.Models.Common;

namespace AppointmentManagerApp.Models.Notification.Requests
{
	public class CreateNotificationResponse : ResponseBase
	{
		public Guid Id { get; set; }
	}
}
