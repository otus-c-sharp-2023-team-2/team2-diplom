﻿namespace AppointmentManagerApp.Models.Notification.Requests
{
	public class NotificationChannelRequest
	{
		Guid UserId { get; set; }
	}
}
