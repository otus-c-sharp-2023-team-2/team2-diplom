﻿namespace AppointmentManagerApp.Models.Notification.Requests
{
    public class CreateNotificationChannelRequest
    {
        public Guid UserId { get; set; }
        public int ChannelType { get; set; }
        public string Adress { get; set; }
    }
}
