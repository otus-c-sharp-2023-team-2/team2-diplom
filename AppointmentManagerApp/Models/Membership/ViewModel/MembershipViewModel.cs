﻿using System.ComponentModel.DataAnnotations;

namespace AppointmentManagerApp.Models.Membership.ViewModel;

public class MembershipViewModel
{
    public Guid UserId { get; set; }

    [Display(Name = "Дата начала")]
    [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:dd.MM.yyyy}")]
    public string StartDate { get; set; }

    [Display(Name = "Дата окончания")]
    public string EndDate { get; set; }

    [Display(Name = "Тип")]
    public string Type { get; set; }

    [Display(Name = "Продолжительность в днях")]
    public int Duration { get; set; }

    public double Price { get; set; }
}
