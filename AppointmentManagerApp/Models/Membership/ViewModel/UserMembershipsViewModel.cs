﻿namespace AppointmentManagerApp.Models.Membership.ViewModel;

public class UserMembershipsViewModel
{
    public Guid UserId { get; set; }

    public IEnumerable<MembershipViewModel> Memberships{ get; set; }
}
