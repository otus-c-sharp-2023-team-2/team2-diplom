﻿namespace AppointmentManagerApp.Models.Membership.Domain;

public class Membership
{
    public Guid UserId { get; set; }
    public string Type { get; set; }
    public int Duration { get; set; }
    public double Price { get; set; }
    public DateTime StartDate { get; set; }
}
