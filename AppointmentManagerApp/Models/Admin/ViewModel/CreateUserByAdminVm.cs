﻿using System.ComponentModel.DataAnnotations;

namespace AppointmentManagerApp.Models.Admin.ViewModel;

public class CreateUserByAdminVm
{
    [Required(ErrorMessage = "Обязательное поле")]
    [Display(Name = "Электронная почта")]
    public string Email { get; set; }

    [Display(Name = "Имя")]
    public string FirstName { get; set; }

    [Display(Name = "Фамилия")]
    public string LastName { get; set; }

    // public IList<Claim> Claims { get; set; }

    public bool? IsCreationSucceed { get; set; }

    public string ErrorMessage { get; set; } = string.Empty;
}
