﻿using AppointmentManagerApp.Models.User.Domain;

namespace AppointmentManagerApp.Models.Admin.ViewModel;

public class UserManagementVm
{
    public IEnumerable<UserFullInfo> Users { get; set; }

    public string SuccessMessage { get; set; }

    public string ErrorMessage { get; set; }
}
