﻿namespace AppointmentManagerApp.Models.Schedule;

public class ScheduleModel
{
    public List<Schedule>? Schedules { get; set; }
}

public class Schedule
{
    public DateTime StartTime { get; set; }

    public DateTime EndTime { get; set; }
}