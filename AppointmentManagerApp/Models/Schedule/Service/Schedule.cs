﻿namespace AppointmentManagerApp.Models.Schedule.Service;

public class Schedule
{
	//public Guid Id { get; set; }

	public DateTime StartTime { get; set; }

	public DateTime EndTime { get; set; }

	public string Trainer { get; set; }
		   
	public string Gym { get; set; }
		   
	public string Customer { get; set; }
}