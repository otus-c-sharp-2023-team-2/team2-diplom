﻿using AppointmentManagerApp.Models.Common;

namespace AppointmentManagerApp.Models.Schedule.Service;

public class GetSchedulesByCustomerResponse : ResponseBase
{
    public List<Schedule> Schedules { get; set; }
}
