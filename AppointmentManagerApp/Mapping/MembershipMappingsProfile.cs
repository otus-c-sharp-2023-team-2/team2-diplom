﻿using AppointmentManagerApp.Models.Admin.ViewModel;
using AppointmentManagerApp.Models.Auth.Requests;
using AppointmentManagerApp.Models.Membership.Domain;
using AppointmentManagerApp.Models.Membership.ViewModel;
using AppointmentManagerApp.Models.User.Domain;
using AppointmentManagerApp.Models.User.Requests;
using AppointmentManagerApp.Models.User.ViewModel;
using AutoMapper;

namespace AppointmentManagerApp.Mapping;

public class MembershipMappingsProfile : Profile
{
    public MembershipMappingsProfile()
    {
        CreateMap<MembershipViewModel, Membership>()
            .ForMember(nameof(Membership.UserId), opt => opt.MapFrom(c => c.UserId))
            .ForMember(nameof(Membership.Type), opt => opt.MapFrom(c => c.Type))
            .ForMember(nameof(Membership.Duration), opt => opt.MapFrom(c => c.Duration))
            .ForMember(nameof(Membership.Price), opt => opt.MapFrom(c => c.Price))
            .ForMember(nameof(Membership.StartDate), opt => opt.MapFrom(c => c.StartDate));

    }
}
