﻿using AppointmentManagerApp.Models.Admin.ViewModel;
using AppointmentManagerApp.Models.Auth.Requests;
using AppointmentManagerApp.Models.User.Domain;
using AppointmentManagerApp.Models.User.Requests;
using AppointmentManagerApp.Models.User.ViewModel;
using AutoMapper;

namespace AppointmentManagerApp.Mapping;

public class UserMappingsProfile : Profile
{
    public UserMappingsProfile()
    {
        CreateMap<UserVm, AuthCreateUserRequest>()
            .ForMember(nameof(AuthCreateUserRequest.Name), opt => opt.MapFrom(c => c.FirstName))
            .ForMember(nameof(AuthCreateUserRequest.LastName), opt => opt.MapFrom(c => c.LastName))
            .ForMember(nameof(AuthCreateUserRequest.Email), opt => opt.MapFrom(c => c.Email))
            .ForMember(nameof(AuthCreateUserRequest.Password), opt => opt.MapFrom(c => c.Password));

        CreateMap<CreateUserByAdminVm, AuthCreateUserRequest>()
            .ForMember(nameof(AuthCreateUserRequest.Name), opt => opt.MapFrom(c => c.FirstName))
            .ForMember(nameof(AuthCreateUserRequest.LastName), opt => opt.MapFrom(c => c.LastName))
            .ForMember(nameof(AuthCreateUserRequest.Email), opt => opt.MapFrom(c => c.Email));

        CreateMap<UserVm, CreateUserRequest>()
            .ForMember(nameof(CreateUserRequest.FirstName), opt => opt.MapFrom(c => c.FirstName))
            .ForMember(nameof(CreateUserRequest.LastName), opt => opt.MapFrom(c => c.LastName))
            .ForMember(nameof(CreateUserRequest.Email), opt => opt.MapFrom(c => c.Email))
            .ForMember(nameof(CreateUserRequest.PhoneNumber), opt => opt.MapFrom(c => c.PhoneNumber))
            .ForMember(nameof(CreateUserRequest.Promocode), opt => opt.MapFrom(c => c.Promocode));

        CreateMap<CreateUserByAdminVm, CreateUserRequest>()
            .ForMember(nameof(CreateUserRequest.FirstName), opt => opt.MapFrom(c => c.FirstName))
            .ForMember(nameof(CreateUserRequest.LastName), opt => opt.MapFrom(c => c.LastName))
            .ForMember(nameof(CreateUserRequest.Email), opt => opt.MapFrom(c => c.Email));

        CreateMap<UserBase, UserVm>()
            .ForMember(nameof(UserVm.FirstName), opt => opt.MapFrom(c => c.FirstName))
            .ForMember(nameof(UserVm.LastName), opt => opt.MapFrom(c => c.LastName))
            .ForMember(nameof(UserVm.Email), opt => opt.MapFrom(c => c.Email))
            .ForMember(nameof(UserVm.PhoneNumber), opt => opt.MapFrom(c => c.PhoneNumber))
            .ForMember(nameof(UserVm.Promocode), opt => opt.MapFrom(c => c.Promocode));
    }
}
