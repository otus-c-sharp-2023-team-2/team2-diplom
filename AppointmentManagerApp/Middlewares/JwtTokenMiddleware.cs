﻿using AppointmentManagerApp.Models.Auth;
using AppointmentManagerApp.Models.Common;
using AppointmentManagerApp.Services.Auth;
using Microsoft.Extensions.Options;

namespace AppointmentManagerApp.Middlewares;

/// <summary>
/// Middleware для установки Bearer заколовка запроса из кукис.
/// </summary>
public class JwtTokenMiddleware : IMiddleware
{
    private readonly AccessTokenSettings _settings;
    private readonly IAuthService _authService;

    public JwtTokenMiddleware(
        IOptions<AccessTokenSettings> settings,
        IAuthService authService)
    {
        _settings = settings?.Value ?? throw new ArgumentNullException(nameof(settings));
        _authService = authService;
    }

    public async Task InvokeAsync(HttpContext context, RequestDelegate next)
    {
        var jwtToken = context.Request.Cookies[AuthConstants.JWT_TOKEN_COOKIE_NAME];

        try
        {
            if (!string.IsNullOrEmpty(jwtToken))
            {
                await AppendAccessToken(context, jwtToken);
            }
        }
        catch (Exception ex)
        {

        }
        finally
        {
            await next(context);
        }

    }

    private async Task AppendAccessToken(HttpContext context, string jwtToken)
    {
        var parsedJwtToken = _authService.ParseJwtToken(jwtToken);

        if (parsedJwtToken.ValidTo > DateTime.UtcNow - TimeSpan.FromSeconds(30))
        {
            context.Request.Headers.Add($"Authorization", $"Bearer {jwtToken}");
            return;
        }

        await RefreshTokenAsync(context, jwtToken);
    }

    private async Task RefreshTokenAsync(HttpContext context, string jwtToken)
    {
        var refreshToken = context.Request.Cookies[AuthConstants.REFRESH_TOKEN_COOKIE_NAME];
        if (string.IsNullOrEmpty(refreshToken))
            return;

        var newTokens = await _authService.RefreshTokenAsync(jwtToken, refreshToken, CancellationToken.None);
        if (!string.IsNullOrEmpty(newTokens?.AccessToken))
            context.Request.Headers.Add($"Authorization", $"Bearer {newTokens.AccessToken}");
    }
}
