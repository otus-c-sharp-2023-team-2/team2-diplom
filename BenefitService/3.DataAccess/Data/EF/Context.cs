﻿using Entities;
using Microsoft.EntityFrameworkCore;

namespace Data.EF;

public class Context : DbContext
{
    public Context()
    {
        Database.EnsureCreated();
    }

    public DbSet<UserEntity> User { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseNpgsql(@"host=host.docker.internal;port=5432;database=BenefitUser;username=root;password=root");
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.UseIdentityColumns();
        base.OnModelCreating(modelBuilder);

        modelBuilder.Entity<UserEntity>()
            .HasData(new UserEntity()
            {
                Id = Guid.NewGuid(),
                 UserId = new Guid("103d165c-6062-4470-a318-09ca1a5350cc"),
                TrainingSessions = 2,
                Subscription = 3,
                UserCode = Guid.NewGuid().ToString(),
                BenefitUser = 5
            });

        modelBuilder.Entity<UserEntity>()
            .HasData(new UserEntity()
            {
                Id = Guid.NewGuid(),
                UserId = new Guid("82f707bb-044e-48e7-af30-67db8bbbb00b"),
                TrainingSessions = 1,
                Subscription = 6,
                UserCode = Guid.NewGuid().ToString(),
                BenefitUser = 5
            });
    }

}