﻿using Data.EF;
using Data.Repositories.Abstract;
using Entities;
using Microsoft.EntityFrameworkCore;

namespace Data.Repositories
{
    public class UserRepository : IUserRepository
    {
        /// <summary>
        /// Добавление клиента при регистрации в DB
        /// </summary>
        /// <param name="user"></param>
        public void AddUser(UserEntity user)
        {
            using (var context = new Context())
            {
                context.User.Add(user);
                context.SaveChanges();
            }
        }

        public void DeleteUserId(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Получаем объект из базы по UserId или UserCode
        /// </summary>
        /// <param name="user"></param>
        /// <param name="status">0 - Ищем в базе клиента по UserId, 1 - Ищем клиента по UserCode</param>
        /// <returns>Возвращает найденный объект</returns>
        public UserEntity GetUser(UserEntity user, int status)
        {
            UserEntity[]? userBenefit = null;
            //Получаем данные для расчета скидки
            using (var context = new Context())
            {
                switch (status)
                {
                    case 0:
                        userBenefit = context.User.Where(x => x.UserId.Equals(user.UserId)).ToArray();
                        break;
                        
                    case 1:
                        userBenefit = context.User.Where(x => x.UserCode.Equals(user.RefCode)).ToArray();
                        break;
                }
                return userBenefit.Length == 0 ? null : userBenefit[0];
            }
        }

        /// <summary>
        /// Обновление клиента при запросе скидки
        /// </summary>
        /// <param name="user"></param>
        /// <param name="status">0 - Добавить скидку в базу, 1 - Очистить данные по скидке</param>
        public void UpdateUser(UserEntity user, int status)
        {
            using (var context = new Context())
            {
                var userBenefit = context.User.Where(x => x.UserId.Equals(user.UserId)).ToArray();

                switch (status)
                {
                    case 0: //Добавить скидку в базу
                            userBenefit[0].BenefitUser = user.BenefitUser;
                            userBenefit[0].TrainingSessions = user.TrainingSessions;
                            userBenefit[0].Subscription = user.Subscription;
                        break;
                    case 1: //Очистить данные по скидке при подтверждении покупки
                            userBenefit[0].BenefitUser = 0;
                            userBenefit[0].TrainingSessions = 0;
                            userBenefit[0].Subscription = 0;
                        break;
                }
                context.SaveChanges();
            }
        }
    }
}

