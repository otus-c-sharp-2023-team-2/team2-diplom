﻿using Entities;

namespace Data.Repositories.Abstract
{
    public interface IUserRepository
    {
        //Добавить пользователя
        void AddUser(UserEntity user);

        //Обновить пользователя
        void UpdateUser(UserEntity user, int status);

        //Получить скидку для пользователя по id
        UserEntity GetUser(UserEntity user, int status);

        //Удалить пользователя по id
        void DeleteUserId(int id);

    }
}
