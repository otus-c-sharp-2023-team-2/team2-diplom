﻿namespace Entities.Abstract
{
    public interface IBaseEntity
    {
        /// <summary>
        /// Идентификатор клиента
        /// </summary>
        public int Id { get; set; }

    }
}
