﻿using Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class AddUserDb
    {
        /// <summary>
        /// Id клиента в системе
        /// </summary>
        public Guid UserId { get; set; }
        
        /// <summary>
        /// Личный Реферальный код клиента
        /// </summary>
        public string UserCode { get; set; } = string.Empty;

        /// <summary>
        /// Реферальный код пригласившего клиента
        /// </summary>
        public string? RefCode { get; set; }
               
    }
}
