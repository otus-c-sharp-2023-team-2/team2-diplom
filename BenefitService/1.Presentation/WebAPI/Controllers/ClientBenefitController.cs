﻿using System.Text.Json;
using Mappers;
using Microsoft.AspNetCore.Mvc;
using Models;
using Services.Abstract;

namespace BenefitWebAPI.Controllers;

[ApiController]
[Route("")]
public class ClientBenefitController : Controller
{
    private readonly IUserService _userService;

    public ClientBenefitController(IUserService userService)
    {
        _userService = userService;
    }

    /// <summary>
    ///     Метод принимает int UserId, string UserCode, string RefCode,
    ///     получаем данные после регистрации пользователя.
    /// </summary>
    /// <param name="user"> модель клиента </param>
    /// <returns></returns>
    [HttpPost("benefit/reg")]
    public async Task<IActionResult> AddUserDb([FromBody] AddUserDb user)
    {
        //Передаём в сервис на добавление клиента в базу
        await _userService.AddUser(user.ToDomain());
        return Ok();

    }

    /// <summary>
    ///     Метод принимает int UserId, int TrainingSessions, int Subscription,
    ///     обновляем данные в базе и считаем скидку.
    /// </summary>
    /// <param name="user"> модель клиента </param>
    /// <returns></returns>
    [HttpPost("benefit/getBenefit")]
    public async Task<IActionResult> GetUserBenefit([FromBody] GetUserBenefit user)
    {
        //Отправка в сервис для расчёта скидки
        var result = await _userService.GetUser(user.ToDomain());
        //Возврат рассчитанной скидки  
        return Ok(result);
    }
}