﻿using Domain;

namespace Services.Abstract
{
    public interface IUserService
    {
        //Добавить пользователя
        Task AddUser(User user);
        //Обновить пользователя
        void UpdateUser(User user);
        //Получить скидку для пользователя по id
        Task<User> GetUser(User user);
        //Удалить пользователя по id
        void DeleteUserId(int id);
    }
}
