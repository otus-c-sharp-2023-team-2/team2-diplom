﻿using Data.Repositories.Abstract;
using Domain;
using Mappers;
using Services.Abstract;

namespace Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        /// <summary>
        /// Добавляем нового клиента в базу
        /// </summary>
        /// <param name="user"></param>
        public Task AddUser(User user)
        {
            //Сохранение в базу данные клиента
            _userRepository.AddUser(user.ToEntity());
            //Проверка RefCode и добавление скидки клиенту
            var result = _userRepository.GetUser(user.ToEntity(), 1);
            if (result != null) 
            {
                result.BenefitUser += 3;
                _userRepository.UpdateUser(result,0);
            }
            return Task.CompletedTask;
        }

        /// <summary>
        /// Удаляем клиента из базы
        /// </summary>
        /// <param name="id"></param>
        /// <exception cref="NotImplementedException"></exception>
        public void DeleteUserId(int id)
        {
            throw new NotImplementedException();
        }


        public async Task<User> GetUser(User user)
        {
            /*  Скидки в %
            *   TrainingSessions > 10  = 5%
            *   Subscription  3 = 4%, 6 = 6%, 12 = 10%
            *   RefCode   3%
            */

            //Получаем данные для расчёта скидки
            var result = _userRepository.GetUser(user.ToEntity(), 0);
            if (result is null)
                return null;

            result.TrainingSessions = user.TrainingSessions;
            result.RefCode = user.RefCode;
            result.Subscription = user.Subscription;

            //Рассчитываем скидку
            int? benefit = 0; 
            if (result.TrainingSessions > 10) benefit += 5;
            if (result.RefCode != null) benefit += 3;
            if (result.BenefitUser != null) benefit += result.BenefitUser;

            switch (result.Subscription)
            {
                case 3:
                    benefit += 4;
                    break;
                case 6:
                    benefit += 6;
                    break;
                case 12:
                    benefit += 10;
                    break;
            }

            result.BenefitUser = benefit;

            //Обновляем в базе данных
            _userRepository.UpdateUser(result,0);

            return result.ToDomain();
        }

        public void UpdateUser(User user)
        {
            throw new NotImplementedException();
        }
    }
}
