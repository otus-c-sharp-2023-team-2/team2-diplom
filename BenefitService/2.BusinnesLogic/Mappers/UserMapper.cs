﻿using Models;
using Domain;
using Entities;

namespace Mappers
{
    public static class UserMapper
    {
        public static UserEntity ToEntity(this User user)
        {
            return new UserEntity
            {
               UserId = user.UserId,
               TrainingSessions = user.TrainingSessions,
               Subscription = user.Subscription,
               UserCode = user.UserCode,
               RefCode = user.RefCode,
               BenefitUser = user.BenefitUser
            };
        }

        public static User ToDomain(this UserEntity user)
        {
            return new User
            {
                UserId = user.UserId,
                TrainingSessions = user.TrainingSessions,
                Subscription = user.Subscription,
                UserCode = user.UserCode,
                RefCode = user.RefCode,
                BenefitUser = user.BenefitUser
            };
        }

        public static User ToDomain(this GetUserBenefit user)
        {
            return new User
            {
                UserId = user.UserId,
                TrainingSessions = user.TrainingSessions,
                Subscription = user.Subscription
            };
        }

        public static User ToDomain(this AddUserDb user)
        {
            return new User
            {
                UserId = user.UserId,
                UserCode = user.UserCode,
                RefCode = user.RefCode
            };
        }
    }
}
