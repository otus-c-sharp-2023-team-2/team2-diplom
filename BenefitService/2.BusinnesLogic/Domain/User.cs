﻿using Entities.Abstract;

namespace Domain
{
    public class User : BaseEntity
    {
        /// <summary>
        /// Id клиента в системе
        /// </summary>
        public Guid UserId { get; set; }
        /// <summary>
        /// Количество купленных тренировок
        /// </summary>
        public int? TrainingSessions { get; set; }

        /// <summary>
        /// Тип абонемента 3/6/12
        /// </summary>
        public int? Subscription { get; set; }

        /// <summary>
        /// Личный Реферальный код клиента
        /// </summary>
        public string UserCode { get; set; }

        /// <summary>
        /// Реферальный код пригласившего клиента
        /// </summary>
        public string? RefCode { get; set; }

        /// <summary>
        /// Скидка клиента
        /// </summary>
        public int? BenefitUser { get; set; }
    }
}
